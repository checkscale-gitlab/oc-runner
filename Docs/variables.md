
## oc_runner variables

For specific Gitlab-ci variables, please refer to the [GITLAB_CI](https://docs.gitlab.com/ce/ci/variables/)

### important variable protection:

the Following Gitlab-CI variables cannot be overwritten by user intervention, and are thus protectect readonly Gitlab-CI system defined:

- `CI_PROJECT_PATH`
- `CI_PROJECT_URL`
- `GITLAB_USER_NAME`
- `GITLAB_USER_EMAIL`
- `GITLAB_USER_ID`
- `GITLAB_USER_LOGIN`
- `CI_REPOSITORY_URL`
- `CI_REGISTRY`
- `CI_REGISTRY_IMAGE`
- `CI_PROJECT_DIR`


### custom defined by oc-runner:

oc-runner set a couple of variables:

- `OC_GIT_VERSION` as in `0.0.33-15-ge2b011bf` is not availlble when `GIT_STRATEGY_NONE` is set.

Info regarding the Build image used:

- `OC_BUILD_IMAGE`
- `OC_BUILD_IMAGE_NICK`
- `OC_BUILD_IMAGE_TAG`
- `OC_BUILD_IMAGE_SHA`


General purpose provided variables:

- `OC_COMMIT_TAG_SLUG` a tag in the form of e.g. `3-1-9`, from the `CI_COMMIT_TAG`: `3.1.9`
-  `OC_REVERSE_PROJECT_PATH*` from e.g.: `oc-runner/build-image` :
    - `OC_REVERSE_PROJECT_PATH_SLUG` => `build-image-oc-runner`(*)
    - `OC_REVERSE_PROJECT_PATH`      => `build-image oc-runner` (*)
- `OC_RUNNER_RFC3339_DATE` e.g. `2018-08-12T08:23:23Z`
- `OC_RUNNER_SHORT_DATE`   e.g. `20180810`
- `nl` is set to `\n` as setting a bash variable with newline is a bit tricky without the use of `echo`

*Note:* if `CI_PROJECT_PATH` contains more then 1 slash, only the last 2 elements are chosen, if the last 2 elements are equal, only the last element is chosen, e.g.:
    
- gioxa/oc-runner/build-image => `build-image oc-runner`
- gioxa/oc-runner/oc-runner => `oc-runner`
- oc-runner/build-image => `build-image oc-runner`

### For autolabeling

registry_push will automatic append labels to the pushed image according to the [org.opencontainer.image](https://github.com/opencontainers/image-spec/blob/master/annotations.md#pre-defined-annotation-keys) labeling scheme.

**Priorities:**

All variables defined in `docker_config.yml` proceed the environment variables

1. docker_config.yml:

    1. `org.opencontainers.image.xxx`
    2. `xxx`
    3. additonal `maintainer` as alternative for `org.opencontainers.image.authors`
    4. additional `license` as alternative for `org.opencontainers.image.licenses`

2. environment
    - `OC_INFO_IMAGE_XXXX`

3. exeptions: the following labels in `docker_config.yml` are **ignored**
    - `org.opencontainers.image.source`
    - `org.opencontainers.image.revision`
    - `org.opencontainers.image.ref.name`

4. `org.opencontainers.image.version`,searched in this order:
    1.  file `docker_config.yml` label: `org.opencontainers.image.version`
    2.  file `docker_config.yml` label: `version`
    3.  environment var: `OC_INFO_IMAGE_VERSION`
    3.  environment var: `OC_GIT_VERSION`
    4.  environment var: `GIT_VERSION`
    5.  environment var: `GITVERSION`
    9.  read-file `$CI_PROJECT_DIR/.VERSION`
    6.  read-file `$CI_PROJECT_DIR/.GIT_VERSION`
    7.  read-file `$CI_PROJECT_DIR/.GITVERSION`
    8.  read-file `$CI_PROJECT_DIR/.tarball-version`


5. `org.opencontainers.image.ref.name`, label in `docker_config.yml` is **ignored**
	- namepart:
        1. environment var : `OC_INFO_IMAGE_REFNAME`
        2. slug( environment var: `OC_INFO_IMAGE_TITLE`)
        3. reverse slug of last 2 sections of `CI_PROJECT_PATH` e.g.:
            - `gioxa/oc-runner/image` becomes `image-oc-runner`
            - `gioxa/oc-runner/oc-runner` becomes `oc-runner`
    - version-part as per above point 4

6. `org.opencontainers.image.title` *if not defined in docker_config.yml*
    1. `OC_INFO_IMAGE_TITLE`
	2. `OC_INFO_IMAGE_NAME`
    3. reverse of last 2 sections of `CI_PROJECT_PATH` e.g.:
        - `gioxa/oc-runner/image` becomes `image oc-runner`
        - `gioxa/oc-runner/oc-runner` becomes `oc-runner`

7.  `org.opencontainers.image.authors` *if not defined in docker_config.yml*
    Read from `$CI_PROJECT_DIR/AUTHORS`, but all `\n` removed (flattened)


#### variables used for label schema

- Custom defined by user variables: *if not defined in docker_config.yml*

    | Label | Variable | note |
| --- | --- | --- |
| org.opencontainers.image.title | `OC_INFO_IMAGE_TITLE` | (1) |
| org.opencontainers.image.licenses | `OC_INFO_IMAGE_LICENSES` | |
| org.opencontainers.image.vendor | `OC_IMAGE_INFO_VENDOR` | |
| org.opencontainers.image.description | `OC_INFO_IMAGE_DESCRIPTION` | (2) |
| org.opencontainers.image.documentation | `OC_INFO_IMAGE_DOCUMENTATION` | |
| org.opencontainers.image.url   | `OC_INFO_IMAGE_URL` | |

    **note**

        1. Also used as default description of `DockerHub_set_description`
        2. Also used as default description of `DockerHub_set_description`

- oc-runner provided variables: *if not defined in docker_config.yml*

    | Label | Variable |
| --- | --- |
| org.opencontainers.image.version |    `OC_INFO_IMAGE_VERSION` |


- gitlab-ci variables

    | Label | Variable |
| --- | --- |
| org.opencontainers.image.source |   `CI_PROJECT_URL` |
| org.opencontainers.image.revision |   `CI_COMMIT_SHA` |

- `io.oc-runner.image`

    | Label | Variable |
| --- | --- |
| io.oc-runner.build.image | `OC_BUILD_IMAGE` |
| io.oc-runner.build.image-nick | `OC_BUILD_IMAGE_NICK` |
| io.oc-runner.build.image-tag | `OC_BUILD_IMAGE_TAG` |
| io.oc-runner.build.image-digest | `OC_BUILD_IMAGE_SHA` |

- `io.oc-runner.build`

    | Label | Variable |
| --- | --- |
| io.oc-runner.build.commit.author | `"%s <%s>", GITLAB_USER_NAME, GITLAB_USER_EMAIL` |
| io.oc-runner.build.commit.id | `CI_COMMIT_SHA` |
| io.oc-runner.build.commit.message | `CI_COMMIT_MESSAGE` |
| io.oc-runner.build.commit.ref | `CI_COMMIT_REF_NAME` |
| io.oc-runner.build.commit.tag | `CI_COMMIT_TAG` |
| io.oc-runner.build.version | `OC_GIT_VERSION` |
| io.oc-runner.build.environment | `CI_ENVIRONMENT_NAME` |
| io.oc-runner.build.job-url | `"%s/-/jobs/%s", CI_PROJECT_URL, CI_JOB_ID` |


### Debugging

Set `OC_RUNNER_DEBUG` to True will enable debug output for the runner.

*Remark:*

If dispatcher is run with `--debug` option, executer will be started with `--debug`

[top](#oc-runner)


