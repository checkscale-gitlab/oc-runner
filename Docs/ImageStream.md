
## ImageStream API

### ImageStream_Delete_Image

Delete an image on the integrated registry on an Openshift Cluster.

```bash
ImageStream_Delete_Image [--name=subname]
```

**Note**

Not all images can be deleted, only those which starts as :

```bash
${OC_RUNNER_NAMESPACE}/is-$CI_PROJECT_PATH_SLUG[-subname]
```

### ImageStream_Delete_Tag

```bash
ImageStream_Delete_Image [--name=name] --reference=tag
```

Delete an ImageStream tag, with the same restriction as with ImageStream_Delete_Image.

[top](#oc-runner)

