# Features
## Feature: dependencies

### Purpose

Selectively retrieving artifacts, base on an variable, e.g. `$CI_JOB_NAME` to avoid downloading uneeded/unwanted artifacts for the Job at hand, reducing the waste  of resources.

### Example:

`.gitlab-ci.yml` build file:
```yaml
.build_tc: &build_tc
  stage: build_stage1
  script:
     - ./make_os "${CI_JOB_NAME#gcc-}"
  artifacts:
     expire_in: 90min
     paths:
       - tc.tar.gz

gcc-el7-x64: *build-tc
gcc-el6-i386: *build-tc
gcc-el6-x64: *build-tc
gcc-el7-arm: *build-tc
gcc-el7-arm64: *build-tc

.build_image: &build_image
  stage: build_image
  variables:
    dependencies: |
                    - "gcc-${CI_JOB_NAME#img-}"
    GIT_STRATEGY: none
  script:
    - merge_tc "${CI_JOB_NAME#img-}"

img-el7-arm: *build_image
img-el7-arm64: *build_image
img-el7-x64: *build_image
img-el6-x64: *build_image
img-el6-i386: *build_image
```

In this example we build toolchains, for different Release / architecture in stage 1,
where in stage 2 we want the artifact of stage 1 which correspond to the release and architecture we need as defined on the trailing part of `${CI_JOB_NAME}`.

For this example, each `gcc-xxx-xxx` creates an artifact of several hundreds of Mega Bytes, which has now been averted by the dependencies variable that filters only what we need.

Thus, only those artifacts from previous builds which match the variable substitution will be downloaded.

**Note**

If the job `dependencies` is set to `[]`, the variable dependencies has no function, we can only filter from what is available.

**Warning** only standard substitution is supported, not extended `wordexp()` substitution.

[top](#oc-runner)

