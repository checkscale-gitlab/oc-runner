
## artifacts extension

### Purpose

- extend gitlab-runner functionality to allow more granulated selection of artifacts files with wildcards `*` and variable substitution.
- speedup donwloading artifacts by parallel downloads to increase download speed on high latency connections.

### wildcard:


The gitlab-ci yaml linter does not allow to use wildcards and/or variable substitution in the artifacts paths, to circomvent, use quotesand define as e.g.:


`.gitlab-ci.yml` build file:
```yaml
build:
  stage: build
  script:
     - ....
  artifacts:
     paths:
       - '${OC_GITVERSION}/*.tar.gz'
```

This will select all `.tar.gz` files in directory `${OC_GIT_VERSION}`

**Note** use quotes or yaml features:

valid notations:
```yaml
paths:
  - >-
       ${OC_GITVERSION}/*.tar.gz
  - '${OC_GITVERSION}/*.tar.gz'
  - "${OC_GITVERSION}/*.tar.gz"

```

### parallel downloads

![artifacts](artifacts.png)

oc-runner has same functionality as [deployctl](www.deployctl.com) for artifacts downloading, , up to 6 threads with progress notification are started to allow fast downloading of artifacts.

 Especially interesting when connection time between build-server and gitlab-server have a high latency connection.

[top](#oc-runner)

