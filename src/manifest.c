//
//  manifest.c
//  cjose_cjson
//
//  Created by Danny Goossen on 23/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "manifest.h"
#include "deployd.h"
#include "b64.h"
#include "jwk.h"
#include "jws.h"


static int manifest_calc_protected(const char * manifest,cJSON ** protected);

char * strip_manifest_v1(const char * manifest)
{
   cJSON * manifest_obj=cJSON_Parse(manifest);
   if (!manifest_obj)
   {
      debug("Not a json serialized object\n");
      return NULL;
   }
   cJSON * schemaVersion=cJSON_GetObjectItem(manifest_obj, "schemaVersion");
   if (!schemaVersion)
   {
      cJSON_Delete(manifest_obj);
      debug("No schemaVersion in manifest\n");
      return NULL;
   }
   if (!((cJSON_IsNumber(schemaVersion) && schemaVersion->valueint == 1 )|| (cJSON_IsString(schemaVersion) && strcmp("1",schemaVersion->valuestring)==0)))
   {
      cJSON_Delete(manifest_obj);
      printf("Not v1 schema\n");
      return NULL;
   }
   cJSON * signatures=cJSON_GetObjectItem(manifest_obj,"signatures" );
   if (!signatures || !cJSON_IsArray(signatures)|| cJSON_GetArraySize(signatures)==0)
   {
      cJSON_Delete(manifest_obj);
      printf("Not a signed manifest, or not array of signatures or array size =0\n");
      return strdup(manifest);
   }
   cJSON * signature=cJSON_GetArrayItem(signatures, 0);
   const char * b64protected=cJSON_get_key(signature, "protected");
   if (!b64protected)
   {
      cJSON_Delete(manifest_obj);
      printf("missing protected\n");
      return NULL;
   }
   unsigned char * protected_dec=NULL;
   size_t outlen=0;
   bool res=base64_decode(b64protected, strlen(b64protected), &protected_dec, &outlen, NULL);
   if (!res || protected_dec==NULL)
   {
      cJSON_Delete(manifest_obj);
      printf("could not decode protected\n");
      return NULL;
   }
   cJSON * protected=cJSON_Parse((const char*)protected_dec);
   if (!protected)
   {
      free(protected_dec);
      cJSON_Delete(manifest_obj);
      printf("could marshall protected\n");
      return NULL;
   }
   cJSON * formatlenth_obj=cJSON_GetObjectItem(protected, "formatLength");
   if (!formatlenth_obj || !cJSON_IsNumber(formatlenth_obj))
   {
      cJSON_Delete(protected);
      free(protected_dec);
      
      cJSON_Delete(manifest_obj);
      printf("could marshall protected formatLength\n");
      return NULL;
   }
   int formatLength=formatlenth_obj->valueint;
   const char * b64formatTail=cJSON_get_key(protected, "formatTail");
   if (!b64formatTail)
   {
      cJSON_Delete(protected);
      free(protected_dec);
      
      cJSON_Delete(manifest_obj);
      printf("could marshall protected formatLength\n");
      return NULL;
   }
   debug("formatLength: %d\n",formatLength);
   size_t formatTail_len=0;
   char * formatTail=NULL;
   res=base64url_decode(b64formatTail, strlen(b64formatTail), (unsigned char **)&formatTail, &formatTail_len, NULL);
   
   if (!res || !formatTail)
   {
      cJSON_Delete(protected);
      protected=NULL;
      free(protected_dec);
      protected_dec=NULL;
      cJSON_Delete(manifest_obj);
      manifest_obj=NULL;
      printf("could decode protected formatTail\n");
      return NULL;
   }
   debug("formatTail:\"%s\"\n",b64formatTail);
   char * NewManifest=calloc(1,formatLength+formatTail_len+1);
   if (NewManifest)
   {
   memcpy(NewManifest, manifest, formatLength);
   memcpy(NewManifest+formatLength, formatTail, formatTail_len);
   }
   free(formatTail);
   cJSON_Delete(protected);
   free(protected_dec);
   
   cJSON_Delete(manifest_obj);
   printf("Newmanifest=\n-->%s<--\n",NewManifest);
   cJSON * New_manifest_obj=cJSON_Parse(NewManifest);
   if (!New_manifest_obj)
   {
      debug ("NewManifest produced is invalid\n");
      free(NewManifest);
      return NULL;
   }
   cJSON_Delete(New_manifest_obj);
   return NewManifest;
}

int manifest_calc_protected(const char * manifest,cJSON ** protected)
{
   *protected=NULL;
   void * p=memrchr(manifest,'}',strlen(manifest));
   if (!p ) return -1;
   p--;
   while(p>(void*)manifest && ((char*)p)[0]==' ')
   {
      p--;
   }
   if (p<=(void*)manifest)
      return -1;
   size_t formatLenth=p-(void*)manifest;

   //p++;
   char * b64formatTail=NULL;
   size_t b64formatTail_len=0;
   base64url_encode(p, strlen(p), &b64formatTail, &b64formatTail_len, NULL);
 
   if (!b64formatTail)
   {
      return -1;
   }
   cJSON * protected_obj=cJSON_CreateObject();
   cJSON_AddNumberToObject(protected_obj,"formatLength",(int) formatLenth);
   cJSON_AddStringToObject(protected_obj, "formatTail", b64formatTail);
   struct tm *tm_gmt;
   time_t t;
   t = time(NULL);
   tm_gmt = gmtime(&t);
   char date_buffer[36];
   strftime(date_buffer, 36, "%Y-%m-%dT%H:%M:%SZ", tm_gmt);
   cJSON_AddStringToObject(protected_obj, "time", date_buffer);
   
   //cJSON_AddStringToObject(protected_obj, "time","2018-01-21T08:58:19Z");
   *protected=protected_obj;
   free(b64formatTail);
   return 0;
}


char * SignManifest(const cJSON *Manifest)
{
   char*result=NULL;
   const char b64Key[]="ewoJImNydiI6ICJQLTI1NiIsCgkiZCI6ICJlVEV4dnREZzl5Sm1sdHhvT21TNm9hVURPQ1J1T3ZvdmxpOFJBS3IxRzdJIiwKCSJraWQiOiAiM0Y2NjpBQ1lVOlk1S0M6RkNQVDozQ1VLOlROTE86M0dGQzpLSU81OjdNWTY6RkdJRTpKRUtTOklCQU8iLAoJImt0eSI6ICJFQyIsCgkieCI6ICJfTWlHRWRvbmZNLXI5M1A2alhBSGRZU0xKdG9YZ3hyS25EdXl4Z3hRaHZNIiwKCSJ5IjogIkg4ZkMtS2V0QUM3TnE2UTFrUmhnWmxJcWdZSUliMUJLc0dqZFNsNDVWR0kiCn0";
   
   char * key_str=NULL;
   size_t key_str_len=0;
   jose_err errrr;
   base64url_decode(b64Key, strlen(b64Key), (unsigned char **)&key_str, &key_str_len, NULL);
   cjose_jwk_t * jwk =cjose_jwk_import(key_str, key_str_len, NULL);
   cjose_jws_t * jws=NULL;
   if (jwk)
   {
      char * Payload_str=cJSON_Print(Manifest);
      cJSON * protected_hdr=NULL;
      if (Payload_str)
      {
         manifest_calc_protected(Payload_str, &protected_hdr);
         if (protected_hdr)
         {
            jws=cjose_jws_sign(jwk,protected_hdr,(unsigned char*)Payload_str, strlen(Payload_str), &errrr);
            cJSON_Delete(protected_hdr);
         }
         free(Payload_str);
      }
   }
   
   if (jws)
   {
      cJSON * Signed_manifest=cJSON_Duplicate(Manifest, 1);
      if (Signed_manifest)
      {
         cJSON * signatures=cJSON_CreateArray();
         cJSON_AddItemToObject(Signed_manifest, "signatures", signatures);
         
           cJSON * signature=cJSON_CreateObject();
           cJSON_AddItemToArray(signatures, signature);
         
             cJSON * header=cJSON_CreateObject();
             cJSON_AddItemToObject(signature, "header", header);
               cJSON_AddItemToObject  (header, "jwk", jwk_to_json(jwk, 0, NULL));
               cJSON_AddStringToObject(header, "alg", "ES256");
         
             cJSON_AddStringToObject_n(signature, "signature", jws->sig_b64u, jws->sig_b64u_len);
             cJSON_AddStringToObject_n(signature, "protected", jws->hdr_b64u, jws->hdr_b64u_len);
         
         
         result=cJSON_Print(Signed_manifest);
         cJSON_Delete(Signed_manifest);
      }
      cjose_jws_release(jws);
   }
   if (jwk) cjose_jwk_release(jwk);
   return result;
}

