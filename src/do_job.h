/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file do_job.h
 *  \brief Header file for the oc_executer do_job
 *  \author Danny Goossen
 *  \date 9/10/17
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#ifndef __deployctl__do_job__
#define __deployctl__do_job__

#include "common.h"

/**
 *  @brief Read job to cJSON from  \a dirpath / \a filename
 *  @param dirpath directory path
 *  @param filename filename to read
 *  @return cJSON struct with the job
 *  @note caller is responsible for freeing the struct
 */
cJSON* read_job_file( const char * dirpath, const char * filename);

/**
 *  @brief do the job as per \a job
 *  @param job the gitlab ci job
 *  @param parameters the global parameters
 *  @return 0 on success of the function, not the job
 */
int do_job(cJSON* job,parameter_t * parameters );

/**
 *  @brief do the SHELL job as per \a job
 *  @param job the gitlab ci job
 *  @param parameters the global parameters
 *  @return 0 on success of the function, not the job
 */
int do_local_shell(cJSON* job,parameter_t * parameters);

#endif /* defined(__deployctl__do_job__) */
