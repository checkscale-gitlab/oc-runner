//
//  cache.c
//  oc-runner
//
//  Created by Danny Goossen on 27/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "deployd.h"
#include "dkr_api.h"
#include "oc_api.h"
#include "cache.h"
#include "specials.h"
#include "openssl.h"
#include "wordexp_var.h"
#include "registry.h"

static const char blank_config_str[]="{\"Hostname\":\"\",\"Domainname\":\"\",\"User\":\"\",\"Memory\":0,\"MemorySwap\":0,\"CpuShares\":0,\"Cpuset\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"PortSpecs\":null,\"ExposedPorts\":null,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[],\"Cmd\":[],\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"NetworkDisabled\":false,\"MacAddress\":\"\",\"OnBuild\":[],\"SecurityOpt\":null,\"Labels\":null}";

static int merge_config(cJSON**config,cJSON*this_config);


int cache_calc_numberoffiles(const char * threshold_path, cJSON* job,char ** errormsg);

int cache_ws_check_push(cJSON * job,cJSON * item,char ** errormsg);
int cache_process_path_obj(cJSON * job,cJSON *cache_path,word_exp_var_t*wev,const char*CI_PROJECT_DIR, char **errormsg);
// for scope group
char * get_gitlab_group_url(const cJSON * env_vars);

int parse_git_cache( struct dkr_api_data_s * apidata, cJSON * job,int local_shell)
{

	int git_skip=0;
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char * cache_strategy=cJSON_get_key(env_vars, "GIT_CACHE_STRATEGY");
	
	
	if (cJSON_GetObjectItem(env_vars,"GIT_STRATEGY") && validate_key(env_vars,"GIT_STRATEGY","none")==0)
		git_skip=1;

	if (!cache_strategy) return 0;
	
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"OC-RUNNER-IMAGESTRAM-IP");
	const char * namespace=cJSON_get_key(sys_vars,"OC-RUNNER-OC-NAMESPACE");
	
	cJSON * cache_pull=cJSON_GetObjectItem(job, "layers_pull");
	cJSON * cache_push=cJSON_GetObjectItem(job, "layers_push");
	cJSON * cache_links=cJSON_GetObjectItem(job, "layers_links");
	cJSON * cache_clean=cJSON_GetObjectItem(job, "layers_clean");
	int layers_pull=0;
	
	const char cache_name[]="git_cache";
	const char cache_path[]=".git";
	
	char * cache_location=NULL;
	const char * ci_project_url=cJSON_get_key(env_vars,"CI_PROJECT_URL");
	const char * branch=cJSON_get_key(env_vars, "CI_COMMIT_REF_SLUG");
	debug("Cache Name: %s path: %s, strategy %s\n",cache_name,cache_path,cache_strategy);
	char image_hash[65];
	if (cJSON_validate_contains_match(env_vars, "GIT_CACHE_STRATEGY", "pull"))
	{
	// check image, for branch to pull, if not exists, pull master, if we need git checkout
		char * image=NULL;
		if (!git_skip)
		{
			
			char * image_cache=NULL;
			asprintf(&image_cache,"%s%s%s",ci_project_url,cache_name,branch);
			if (image_cache)
			{
				calc_sha256_digest_hash (image_cache,0, image_hash);
				free(image_cache);
				image_cache=NULL;
				asprintf(&image,"%s/%s/is-executer-image:git-cache-%s", ImageStream_domain,namespace,image_hash);
				asprintf(&cache_location,"/cache/%s",image_hash);
			}
			if((local_shell && (v_exist_dir("%s",cache_location) ||strcmp(branch, "master")==0))|| (!local_shell&& image && dkr_api_manifest_exists(apidata, image,NULL,NULL)==200))
			{
				if (!local_shell && !cache_pull) cache_pull=cJSON_CreateArray();
				if(!local_shell && cache_pull)
				{
					cJSON * link=cJSON_CreateObject();
					cJSON_AddStringToObject(link, "name" ,cache_name);
					cJSON_AddStringToObject(link, "image" ,image);
					cJSON_AddFalseToObject(link, "mandatory");
					cJSON_AddItemToArray(cache_pull, link);
				}
				layers_pull++;
				//need to add item to linkdir
				cJSON * link=cJSON_CreateObject();
				if (!cache_links) cache_links=cJSON_CreateArray();
				cJSON_AddStringToObject(link, "name",cache_name);
				cJSON_AddStringToObject(link, "path" , cache_path);
				if(local_shell)
				{
					cJSON_AddStringToObject(link, "location",cache_location);
					if (!v_exist_dir("%s",cache_location)) vmkdir("%s",cache_location);
				}
				cJSON_AddItemToArray(cache_links, link);
				free(cache_location);
				cache_location=NULL;
				
			}
			else if (strcmp(branch, "master")!=0)
			{
				debug ("cache not master\n");
				char * image_cache_master=NULL;
				char * cache_location_master=NULL;
				asprintf(&image_cache_master,"%s%s%s",ci_project_url,cache_name,"master");
				char image_hash_master[65];
				if (image_cache_master)
				{
					calc_sha256_digest_hash (image_cache_master,0, image_hash_master);
					free(image_cache_master);
					image_cache_master=NULL;
					asprintf(&cache_location_master,"/cache/%s",image_hash);
				}
				if (local_shell && !v_exist_dir("/cache/%s",image_hash_master))
				{
					// TODO "cp -r /cache/image_master_hash /cache/<image_hash>" copy master to branch preserve timestamps
					vmkdir("%s",cache_location);
					char * cp_command=NULL;
					asprintf(&cp_command, "cp -aR %s/ %s/",cache_location_master,cache_location);
					
					int res=system(cp_command);
					if (cp_command) free(cp_command);
					if (res)
						debug("copy branch to master failed, no cache for branch\n");
				}
				else if (local_shell)
				{
					vmkdir("%s",cache_location);
				}
				if (image) free(image);
				image=NULL;
				asprintf(&image,"%s/%s/is-executer-image:git-cache-%s", ImageStream_domain,namespace,image_hash_master);
				if(local_shell || ( !local_shell && image &&dkr_api_manifest_exists(apidata, image,NULL,NULL)==200))
				{
					if (!local_shell && !cache_pull) cache_pull=cJSON_CreateArray();
					if(!local_shell && cache_pull)
					{
						cJSON * link=cJSON_CreateObject();
						cJSON_AddStringToObject(link, "name" ,cache_name);
						cJSON_AddStringToObject(link, "image" ,image);
						cJSON_AddFalseToObject(link, "mandatory");
						cJSON_AddItemToArray(cache_pull, link);
					}
					layers_pull++;
					if (cache_links) cJSON_CreateArray();
					//need to add item to linkdir
					cJSON * link=cJSON_CreateObject();
					if (!cache_links) cache_links=cJSON_CreateArray();
					cJSON_AddStringToObject(link, "name" ,cache_name);
					cJSON_AddStringToObject(link, "path" ,cache_path);
					if(local_shell)
					{
						cJSON_AddStringToObject(link, "location",cache_location); //dest
					}
					cJSON_AddItemToArray(cache_links, link);
				}
			}
			else debug("No git cache\n");
			
			if (image) free(image);
			image=NULL;
		}
		
	}
	if ((strcmp(branch, "master")==0  ) && cJSON_validate_contains_match(env_vars, "GIT_CACHE_STRATEGY", "push"))
	{
		debug("prepare git push list\n");
		if (!local_shell && !cache_push) cache_push=cJSON_CreateArray();
		if (!cache_links) cache_links=cJSON_CreateArray();
		char * image_cache=NULL;
		const char * branch=cJSON_get_key(env_vars, "CI_COMMIT_REF_SLUG");
		asprintf(&image_cache,"%s%s%s",cJSON_get_key(env_vars,"CI_PROJECT_URL" ),cache_name,branch);
		
		if(image_cache)
		{
			calc_sha256_digest_hash (image_cache,0, image_hash);
			free(image_cache);
			image_cache=NULL;
		}
		char * image=NULL;
		
			asprintf(&image,"%s/%s/is-executer-image:git-cache-%s", ImageStream_domain,namespace,image_hash);
		debug("push image: %s, local_shell=%d\n",image,local_shell);
		char * link_path=NULL;
		if (!cache_push) cache_push=cJSON_CreateArray();
		cJSON * item=cJSON_CreateObject();
		cJSON_AddStringToObject(item, "name"  , cache_name);
		cJSON_AddStringToObject(item, "image" , image);
	    cJSON_AddStringToObject(item, "path"  , cache_path);
		if(local_shell)
		{
			
			asprintf(&link_path,"/cache/%s",image_hash);
			if (link_path)
			{
				cJSON_AddStringToObject(item, "location",link_path);
				if (!v_exist_dir("%s",link_path)) vmkdir("%s",link_path);
			}
		}
		else
		{
			cJSON_AddStringToObject(item, "image_name","is-executer-image");
		}
		if (item && cache_push)
			cJSON_AddItemToArray(cache_push,item);
		item=NULL;
		if (image) free(image);
		image=NULL;
		int have_link=0;
		cJSON * link=NULL;
		if (cache_links)
			cJSON_ArrayForEach(link, cache_links)
		{
			const char * link_name=cJSON_get_key(link, "name");
			if (link_name && strcmp(link_name,cache_name)==0)
			{
				have_link=1;
				break;
			}
		}
		//need to add item to linkdir
		if (!have_link)
		{
			if (local_shell || strcmp(cache_name,"git_cache")==0)
			{
				debug("adding cache links, name:%s, path:%s, location:%s\n",cache_name,cache_path,link_path);
				if(!cache_links) cache_links=cJSON_CreateArray();
				if (cache_links)
				{
					link=cJSON_CreateObject();
					if (link)
					{
						cJSON_AddStringToObject(link, "name"      ,cache_name);
						cJSON_AddStringToObject(link, "path"      ,cache_path);
						if (link_path)
							cJSON_AddStringToObject(link, "location"      ,link_path);
						cJSON_AddItemToArray(cache_links, link);
					}
				}
			}
		}
		if (link_path) free(link_path);
		link_path=NULL;
	}
	if (cache_strategy && cJSON_validate_contains_match(env_vars, "GIT_CACHE_STRATEGY", "clean") )
	{
		debug("create cache clean list\n");
		char * image_cache=NULL;
		const char * branch=cJSON_get_key(env_vars, "CI_COMMIT_REF_SLUG");
		asprintf(&image_cache,"%s%s%s",ci_project_url,cache_name,branch);
		
		if(image_cache)
		{
			calc_sha256_digest_hash (image_cache,0, image_hash);
			free(image_cache);
			image_cache=NULL;
		}
		if (!cache_clean) cache_clean=cJSON_CreateArray();
		if(cache_clean)
		{
			cJSON * link=cJSON_CreateObject();
			cJSON_AddStringToObject(link, "name" ,cache_name);
			
			if (local_shell)
			{
				char * link_path=NULL;
				asprintf(&link_path,"/cache/%s",image_hash);
				if (link_path)
				{
					cJSON_AddStringToObject(link, "location"  ,link_path);
					free(link_path);
				}
			}
			else
			{
				char * ocapi=NULL;
				asprintf(&ocapi, "/oapi/v1/namespaces/%s/imagestreamtags/is-executer-image%%3Agit-cache-%s",namespace,image_hash);
				if (ocapi)
				{
					
					cJSON_AddStringToObject(link, "ocapi" ,ocapi);
					free(ocapi);
				}
			}
			cJSON_AddItemToArray(cache_clean, link);
		}
	}
	if (cache_location) free(cache_location);
	cache_location=NULL;
	
	if(!cJSON_GetObjectItem(job, "layers_pull") && cache_pull) cJSON_AddItemToObject(job, "layers_pull", cache_pull);
	if(!cJSON_GetObjectItem(job, "layers_push") && cache_push) cJSON_AddItemToObject(job, "layers_push", cache_push);
	if(!cJSON_GetObjectItem(job, "layers_links") && cache_links) cJSON_AddItemToObject(job, "layers_links", cache_links);
	if(!cJSON_GetObjectItem(job, "layers_clean") && cache_clean) cJSON_AddItemToObject(job, "layers_clean", cache_clean);
	debug("cache_pull list:\n");
	print_json(cache_pull);
	debug("cache_push list:\n");
	print_json(cache_push);
	debug("cache_links list:\n");
	print_json(cache_links);
	return layers_pull;
}




int parse_workspaces( struct dkr_api_data_s * apidata, cJSON * job,int local_shell)
{
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
    
    cJSON * workspaces=cJSON_GetObjectItem(env_vars, "WORK_SPACES");
    if ( workspaces && cJSON_IsString(workspaces) )
    {
        cJSON * temp=yaml_sting_2_cJSON(NULL,workspaces->valuestring );
        if (temp && cJSON_IsArray(temp) )
        {
            workspaces=cJSON_GetArrayItem(temp, 0);
        }
        else
            workspaces=NULL;
    }
    else
        workspaces=NULL;
    
    print_json(workspaces);
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"OC-RUNNER-IMAGESTRAM-IP");
	const char * namespace=cJSON_get_key(sys_vars,"OC-RUNNER-OC-NAMESPACE");
	
	cJSON * workspace_pull=cJSON_GetObjectItem(job, "layers_pull");
	cJSON * workspace_push=cJSON_GetObjectItem(job, "layers_push");
	cJSON * workspace_links=cJSON_GetObjectItem(job, "layers_links");
	cJSON * workspace_clean=cJSON_GetObjectItem(job, "layers_clean");
	//cJSON_AddItemToObject(job, "image-workspace", workspace);
	cJSON * workspace=NULL;
	int layers_pull=0;
	const char * ci_project_url=cJSON_get_key(env_vars,"CI_PROJECT_URL");
	const char * ci_pipeline_id=cJSON_get_key(env_vars,"CI_PIPELINE_ID");
	char * ci_group_url= get_gitlab_group_url(env_vars);
	
	cJSON_ArrayForEach(workspace, workspaces)
	{
        debug("workspace\n");
        print_json(workspace);
		char * workspace_name=resolve_const_vars(cJSON_get_key(workspace, "name"),env_vars);
		if(!workspace_name) continue;
		char * workspace_key=resolve_const_vars(cJSON_get_key(workspace, "key"),env_vars);
		cJSON * workspace_path=cJSON_GetObjectItem(workspace, "path");
		char * workspace_scope=resolve_const_vars(cJSON_get_key(workspace, "scope"),env_vars);
		char * workspace_strategy=resolve_const_vars(cJSON_get_key(workspace, "strategy"),env_vars);
		int not_mandatory=cJSON_safe_IsFalse(workspace, "mandatory");
		int is_mandatory=cJSON_safe_IsTrue(workspace, "mandatory");
		char * workspace_name_slug=NULL;
		// optional, not ready for that yet,
		// char * workspace_path_root=resolve_const_vars(cJSON_get_key(workspace_item, "path_root"),env_vars);
		
		workspace_name_slug=slug_it(workspace_name, 0);
		if (!workspace_scope)
			asprintf(&workspace_scope, "pipeline");
		
		
		
		debug("Cache Name: %s strategy %s\n",workspace_name,workspace_strategy);
		char image_hash[65];
		image_hash[64]='\0';
		int mandatory=0;
		char * image_workspace=NULL;
		{
			
			if  (workspace_scope && strcmp(workspace_scope,"pipeline")==0)
			{
				asprintf(&image_workspace,"%s%s%s%s",ci_pipeline_id,ci_project_url,workspace_name,workspace_key);
				if (!not_mandatory) mandatory=1;
			}
			else if  (workspace_scope && strcmp(workspace_scope,"project")==0)
			{
				asprintf(&image_workspace,"%s%s%s",ci_project_url,workspace_name,workspace_key);
				if(is_mandatory) mandatory=1;
			}
			else if (workspace_scope && strcmp(workspace_scope,"group")==0)
			{
				asprintf(&image_workspace,"%s%s%s", ci_group_url,workspace_name,workspace_key);
				if(is_mandatory) mandatory=1;
			}
			else if (workspace_scope && strcmp(workspace_scope,"global")==0)
			{
				asprintf(&image_workspace,"%s%s",workspace_name,workspace_key);
				if(is_mandatory) mandatory=1;
			}
			else
			{
				asprintf(&image_workspace,"%s%s%s",workspace_name,workspace_scope,workspace_key);
				if(is_mandatory) mandatory=1;
			}
		}
		char * image=NULL;
		if(image_workspace)
		{
			calc_sha256_digest_hash (image_workspace,0, image_hash);
			free(image_workspace);
			image_workspace=NULL;
			asprintf(&image,"%s/%s/is-executer-image:ws-%s-%s", ImageStream_domain,namespace,workspace_name_slug,image_hash);
		}
		
		if (workspace_name)
		{
			if (!workspace_strategy || (workspace_strategy && (strstr( workspace_strategy,"pull"))))
			{
				int exists=0;
				if(local_shell || (!local_shell&& image && (exists=dkr_api_manifest_exists(apidata, image,NULL,NULL))==200) || mandatory)
				{
					if (!local_shell && !workspace_pull) workspace_pull=cJSON_CreateArray();
					if(!local_shell && workspace_pull)
					{
						cJSON * link=cJSON_CreateObject();
						cJSON_AddStringToObject(link, "name" ,workspace_name);
						cJSON_AddStringToObject(link, "image" ,image);
						if (mandatory)
							cJSON_AddTrueToObject(link, "mandatory");
						else
							cJSON_AddFalseToObject(link, "mandatory");
						if (exists!=200 && mandatory && !local_shell)
						{
							cJSON_AddTrueToObject(link, "missing");
						}
						cJSON_AddItemToArray(workspace_pull, link);
					}
					layers_pull++;
			
					if (local_shell )
					{
						char * link_path=NULL;
						if(!workspace_links) workspace_links=cJSON_CreateArray();
						cJSON * link=cJSON_CreateObject();
						cJSON_add_string(link, "name" ,workspace_name);
						if (workspace_path)cJSON_AddItemToObject(link, "path" ,cJSON_Duplicate( workspace_path,1));
						asprintf(&link_path,"/workspaces/%s",image_hash);
						if (link_path)
							cJSON_add_string(link, "location"  ,link_path);
						cJSON_AddItemToArray(workspace_links, link);
					}
				}
				//if (local_shell && !v_exist_dir("/workspace/%s/%s",image_hash,workspace_name)) vmkdir("/workspace/%s",image_hash,workspace_name);
				
			}
		}
		if (workspace_strategy && (strstr( workspace_strategy,"push")))
		{
			cJSON* threshold=cJSON_GetObjectItem(workspace, "threshold");
			cJSON* wp_env=cJSON_GetObjectItem(workspace, "environment");
			if (!local_shell && !workspace_push) workspace_push=cJSON_CreateArray();
			if (!workspace_links) workspace_links=cJSON_CreateArray();
			debug("push image: %s, local_shell=%d\n",image,local_shell);
			char * link_path=NULL;
			if (!workspace_push) workspace_push=cJSON_CreateArray();
			cJSON * item=cJSON_CreateObject();
			cJSON_add_string(item, "name"  , workspace_name);
			cJSON_add_string(item, "image" , image);
			if (workspace_path) cJSON_AddItemToObject(item, "path" ,cJSON_Duplicate( workspace_path,1));
			if (threshold && strcmp( workspace_strategy,"push-pull")==0) cJSON_AddItemToObject(item, "threshold",cJSON_Duplicate( threshold,1));
			if(wp_env) cJSON_AddItemToObject(item, "environment",cJSON_Duplicate( wp_env,1));
			if(local_shell)
			{
				asprintf(&link_path,"/workspace/%s",image_hash);
				if (link_path)
				{
					cJSON_AddStringToObject(item, "location",link_path);
					if (!v_exist_dir("%s",link_path)) vmkdir("%s",link_path);
				}
			}
			else
				cJSON_AddStringToObject(item, "image_name","is-executer-image");
			cJSON_AddItemToArray(workspace_push,item);
			item=NULL;
			int have_link=0;
			cJSON * link=NULL;
			if (workspace_links)
				cJSON_ArrayForEach(link, workspace_links)
			{
				const char * link_name=cJSON_get_key(link, "name");
				if (link_name && strcmp(link_name,workspace_name)==0)
				{
					have_link=1;
					break;
				}
			}
			//need to add item to linkdir
			if (!have_link)
			{
				if (local_shell)
				{
					if(!workspace_links) workspace_links=cJSON_CreateArray();
					link=cJSON_CreateObject();
					cJSON_add_string(link, "name"      ,workspace_name);
					if (workspace_path)
					{
						cJSON_AddItemToObject(link, "path" ,cJSON_Duplicate( workspace_path,1));
					// todo need fixing for local shell
						cJSON_AddItemToObject(item, "location" ,cJSON_Duplicate( workspace_path,1));
					}
					cJSON_AddItemToArray(workspace_links, link);
				}
			}
			if (link_path) free(link_path);
		}
		if (workspace_strategy && (strstr( workspace_strategy,"clean")))
		{
			
			if (!workspace_clean) workspace_clean=cJSON_CreateArray();
			if(workspace_clean)
			{
				cJSON * link=cJSON_CreateObject();
				cJSON_AddStringToObject(link, "name" ,workspace_name);
				
				if (local_shell)
				{
					char * link_path=NULL;
					asprintf(&link_path,"/workspaces/%s",image_hash);
					if (link_path)
					{
						cJSON_AddStringToObject(link, "location"  ,link_path);
						free(link_path);
					}
				}
				else
				{
					char * ocapi=NULL;
					debug("imageHash %s",image_hash);
					asprintf(&ocapi, "/oapi/v1/namespaces/%s/imagestreamtags/is-executer-image%sws-%s-%s",namespace,"%3A",workspace_name_slug,image_hash);
					if (ocapi)
					{
					
						cJSON_AddStringToObject(link, "ocapi" ,ocapi);
						free(ocapi);
						ocapi=NULL;
					}
				}
				cJSON_AddItemToArray(workspace_clean, link);
			}
			
		}
		if(workspace_name_slug) free(workspace_name_slug);
		workspace_name_slug=NULL;
		if (image_workspace) free(image_workspace);
		image_workspace=NULL;
		if (image) free(image);
		image=NULL;
		if (workspace_name) free(workspace_name);
		workspace_name=NULL;
		if (workspace_key) free(workspace_key);
		workspace_key=NULL;
		if (workspace_path) free(workspace_path);
		workspace_path=NULL;
		if (workspace_scope) free(workspace_scope);
		workspace_scope=NULL;
		if (workspace_strategy) free(workspace_strategy);
		workspace_strategy =NULL;
	}
	if (ci_group_url) free(ci_group_url);
	ci_group_url=NULL;
	
	if(!cJSON_GetObjectItem(job, "layers_pull") && workspace_pull) cJSON_AddItemToObject(job, "layers_pull", workspace_pull);
	if(!cJSON_GetObjectItem(job, "layers_push") && workspace_push) cJSON_AddItemToObject(job, "layers_push", workspace_push);
	if(!cJSON_GetObjectItem(job, "layers_links") && workspace_links) cJSON_AddItemToObject(job, "layers_links", workspace_links);
	if(!cJSON_GetObjectItem(job, "layers_clean") && workspace_clean) cJSON_AddItemToObject(job, "layers_clean", workspace_clean);
	debug("workspace_pull\n");
	print_json(workspace_pull);
	debug("workspace_push\n");
	print_json(workspace_push);
	debug("workspace_links\n");
	print_json(workspace_links);
	return layers_pull;
}

// TODO fix config

int append_image_2_layer_data( void * apidata,const char* image, cJSON ** layer_data ,int * schemaVersion,const char * display_name)
{
	if (!layer_data || !image || !apidata) return -1;
	
	cJSON * v2_conf_history=NULL;
	cJSON * v2_conf_diff_ids=NULL;
	cJSON * v2_manifest_layers=NULL;
	cJSON * v1_manifest_fslayers=NULL;
	cJSON * v1_manifest_history=NULL;
	cJSON * v2_conf_config=NULL;
	cJSON * from_image=NULL;
	cJSON * display_names=NULL;
	int res=0;
	if (!*layer_data)
	{
		*layer_data=cJSON_CreateObject();
		cJSON_AddNumberToObject(*layer_data, "schemaVersion",*schemaVersion);
	}
	else
	{
		display_names=cJSON_GetObjectItem(*layer_data,"display_names");
		from_image=cJSON_GetObjectItem(*layer_data,"from_image");
		if (*schemaVersion==1)
		{
			v1_manifest_fslayers=cJSON_GetObjectItem(*layer_data,"fsLayers");
			v1_manifest_history=cJSON_GetObjectItem(*layer_data,"history");
		}
		else
		{
			v2_conf_config=cJSON_GetObjectItem(*layer_data,"conf_config");
			v2_conf_history=cJSON_GetObjectItem(*layer_data, "conf_history");
			v2_conf_diff_ids=cJSON_GetObjectItem(*layer_data, "config_diff_ids");
			v2_manifest_layers=cJSON_GetObjectItem(*layer_data, "layers");
		}
		
	}
		cJSON * manifest=NULL;
	
	 
	
	if (*schemaVersion==1)
		res=dkr_api_get_manifest(apidata, "application/vnd.docker.distribution.manifest.v1+json", image, &manifest);
	else
		res=dkr_api_get_manifest(apidata, "application/vnd.docker.distribution.manifest.v2+json", image, &manifest);
	if (res==200) res=0;
	if (!res && manifest)
	{
		int this_schemaVerson=0;
		debug("\n-------------------------\nappend manifest V%d:%s\n----------------------------\n",*schemaVersion,image);
		print_json(manifest);
	    cJSON * schema=cJSON_GetObjectItem(manifest, "schemaVersion");
		if ( schema && cJSON_IsNumber(schema)) this_schemaVerson=schema->valueint;
		else if (schema && cJSON_IsString(schema) ) debug("schemaVersion is string, take action Danny\n");
		// if first run and build image schemaVersion=1 while registry supports schemaVerson=2, we set schemaVersion to 1
		if (!cJSON_GetObjectItem(*layer_data, "schemaVersion") && *schemaVersion==2 && this_schemaVerson==1) *schemaVersion=1;
		// unlike above, we have a problem, already loaded v2 image(s), so exist and place a layer up in a `Do while and re-add all images`
		if (cJSON_GetObjectItem(*layer_data, "schemaVersion") && *schemaVersion==2 && this_schemaVerson==1)
		{
			if (*layer_data) cJSON_Delete(*layer_data);
			*layer_data=NULL;
			cJSON_Delete(manifest);
			manifest=NULL;
			*schemaVersion=1;
			return -2;
		}
	
	
		if ( this_schemaVerson)
		{
			debug("append: v2_manifest->%s\n",image);
			if (this_schemaVerson==2)
			{
				cJSON * config=NULL;
				res=dkr_api_get_manifest(apidata, "application/vnd.docker.container.image.v1+json", image, &config);
				if (res==200) res=0;
				if (config)
				{
					
					if (cJSON_GetObjectItem(config, "schemaVersion") && !cJSON_GetObjectItem(config, "rootfs"))
					{
						debug("problem, no config, got manifest schemaVersion %d\n",cJSON_safe_GetNumber(config, "schemaVersion"));
						cJSON_Delete(config);
						config=NULL;
						char * config_str=NULL;
						const char *  config_blob=cJSON_get_key (cJSON_GetObjectItem(manifest, "config"),"digest");
						if (config_blob)dkr_api_blob_get(apidata, image, config_blob, &config_str);
						if (config_str)
						{
							config=cJSON_Parse(config_str);
							if(config_str) free(config_str);
							config_str=NULL;
						}
					}
				}
				if (config)
				{
					cJSON * rootfs=cJSON_GetObjectItem(config, "rootfs");
					
					cJSON * hists=cJSON_GetObjectItem(config, "history");
					cJSON * hist=NULL;
					if (hists)cJSON_ArrayForEach(hist, hists)
					{
						if (!v2_conf_history)
						{
							v2_conf_history=cJSON_CreateArray();
							cJSON_AddItemToObject(*layer_data, "conf_history", v2_conf_history);
						}
						if (v2_conf_history) cJSON_AddItemToArray(v2_conf_history, cJSON_Duplicate(hist, 1));
					}
					
					cJSON * diff_ids=NULL;
					if (rootfs)
						diff_ids=cJSON_GetObjectItem(rootfs, "diff_ids");
					cJSON * diff_id=NULL;
					if (diff_ids) cJSON_ArrayForEach(diff_id, diff_ids)
					{
						if (!v2_conf_diff_ids)
						{
							v2_conf_diff_ids=cJSON_CreateArray();
							cJSON_AddItemToObject(*layer_data, "config_diff_ids", v2_conf_diff_ids);
						}
						if (v2_conf_diff_ids) cJSON_AddItemToArray(v2_conf_diff_ids, cJSON_Duplicate(diff_id, 1));
					}
					// config-config merge
					cJSON*this_config_conf=cJSON_GetObjectItem(config, "config");
					debug("\nthis config V2\n");
					cJSON* thisenv=cJSON_GetObjectItem(this_config_conf, "Env");
					cJSON * l_env=cJSON_GetObjectItem(*layer_data, "ws_environment");
					if (thisenv)
					{
						
						if (!l_env)
						{
							l_env=cJSON_CreateObject();
							cJSON_AddItemToObject(*layer_data, "ws_environment", l_env);
						}
						cJSON_append_env_obj_from_dkr_Env(&l_env, thisenv);
					}
					if (strstr(display_name,"WS:")==display_name && this_config_conf)
					{
						// delete env
						cJSON_DeleteItemFromObject(this_config_conf, "Env");
						thisenv=cJSON_CreateArray();
						cJSON_AddItemToObject(this_config_conf, "Env", thisenv);
					}
					if (!v2_conf_config)
					{
						merge_config(&v2_conf_config,this_config_conf);
						cJSON_AddItemToObject(*layer_data, "conf_config", v2_conf_config);
					}
					else
						merge_config(&v2_conf_config,this_config_conf);
					//print_json(v2_conf_config);
					// remove container
					// remove CMD / ENTRYPOINT / USER / VOLUMES
					cJSON_Delete(config);
					config=NULL;
				}
				else res=-1;
				if (!res)
				{
				cJSON * layers=cJSON_GetObjectItem(manifest, "layers");
				cJSON * layer=NULL;
				if (layers)cJSON_ArrayForEach(layer, layers)
				{
					if (!v2_manifest_layers)
					{
						v2_manifest_layers=cJSON_CreateArray();
						cJSON_AddItemToObject(*layer_data, "layers", v2_manifest_layers);
					}
					if (v2_manifest_layers) cJSON_AddItemToArray(v2_manifest_layers, cJSON_Duplicate(layer, 1));
					if(!from_image)
					{
						from_image=cJSON_CreateArray();
						cJSON_AddItemToObject(*layer_data, "from_image", from_image);
						
					}
					if(from_image) cJSON_AddItemToArray(from_image, cJSON_CreateString(image));
					if(!display_names)
					{
						display_names=cJSON_CreateArray();
						cJSON_AddItemToObject(*layer_data, "display_names", display_names);
						
					}
					if(display_names) cJSON_AddItemToArray(display_names, cJSON_CreateString(display_name));
				}
				}
			}
			else if (this_schemaVerson==1)
			{
				cJSON * layers=cJSON_GetObjectItem(manifest, "fsLayers");
				cJSON * layer=NULL;
				// can't use foreach, we need to reverse
				int i=0;
				for (i=cJSON_GetArraySize(layers)-1;i>-1;i--)
				{
					layer=cJSON_GetArrayItem(layers, i);
					if (!v1_manifest_fslayers)
					{
						v1_manifest_fslayers=cJSON_CreateArray();
						cJSON_AddItemToObject(*layer_data, "fsLayers", v1_manifest_fslayers);
					}
					if (v1_manifest_fslayers) cJSON_AddItemToArray(v1_manifest_fslayers, cJSON_Duplicate(layer, 1));
					if(!from_image)
					{
						from_image=cJSON_CreateArray();
						cJSON_AddItemToObject(*layer_data, "from_image", from_image);
						
					}
					if(from_image) cJSON_AddItemToArray(from_image, cJSON_CreateString(image));
					if(!display_names)
					{
						display_names=cJSON_CreateArray();
						cJSON_AddItemToObject(*layer_data, "display_names", display_names);
						
					}
					if(display_names) cJSON_AddItemToArray(display_names, cJSON_CreateString(display_name));
				}
				char * parent=NULL;
				cJSON * v1_hist_config=NULL;
				if (v1_manifest_history)
				{
					//TODO, problem here for V1, need to update parent!!!
					// extract from last added image the id and add//replace that id to this history::v1Compatibility parsed
					// so first get latest one
					int cnt=cJSON_GetArraySize(v1_manifest_history);
					if (cnt)
					{
						cJSON * last_v1_hist=cJSON_GetArrayItem(v1_manifest_history, cnt-1);
						if (last_v1_hist)
						{
							const char * v1comp_str=cJSON_get_key(last_v1_hist, "v1Compatibility");
							if (v1comp_str)
							{
								cJSON * v1comp=cJSON_Parse(v1comp_str);
								if (v1comp)
								{
									// id is not in V1compat config but v1compat
									parent=cJSON_get_stringvalue_dup(v1comp, "id");
									// we also need the environment of the config!
									cJSON* config=cJSON_GetObjectItem(v1comp, "config");
									if(config)
										v1_hist_config=cJSON_Duplicate(config,1);
									cJSON_Delete(v1comp);
									v1comp=NULL;
								}
								v1comp_str=NULL;
							}
						}
					}
				}
				
				cJSON * hists=cJSON_GetObjectItem(manifest, "history");
				cJSON * hist=NULL;
				if (hists)
				{
					for (i=cJSON_GetArraySize(hists)-1;i>-1;i--)
					{
						hist=cJSON_GetArrayItem(hists, i);
						if (!v1_manifest_history)
						{
							v1_manifest_history=cJSON_CreateArray();
							cJSON_AddItemToObject(*layer_data, "history", v1_manifest_history);
						}
						
							// now dup histlayer and insert/replace parent
							// new layer
						cJSON * newhist=cJSON_Duplicate(hist, 1);
						if (newhist)
						{
							
							const char *  v1comp_str=cJSON_get_key(newhist, "v1Compatibility");
							if (v1comp_str)
							{
								cJSON * v1comp=cJSON_Parse(v1comp_str);
								if (v1comp)
								{
									if (parent)
									{
										debug("set Parent pass %d,for  %s\n",i,image);
										cJSON_safe_addString2Obj(v1comp, "parent", parent);
										free(parent);
										parent=NULL;
									}
									cJSON_DeleteItemFromObject(v1comp, "container");
									cJSON_DeleteItemFromObject(v1comp, "container_config");
									if (i==0)
									{
										cJSON* this_config=cJSON_DetachItemFromObject(v1comp, "config");
						
										
										cJSON* thisenv=cJSON_GetObjectItem(this_config, "Env");
										cJSON * l_env=cJSON_GetObjectItem(*layer_data, "ws_environment");
										if (thisenv)
										{
											if (!l_env)
											{
												l_env=cJSON_CreateObject();
												cJSON_AddItemToObject(*layer_data, "ws_environment", l_env);
											}
											cJSON_append_env_obj_from_dkr_Env(&l_env, thisenv);
										}
										if (strstr(display_name,"WS:")==display_name && this_config)
										{
											// delete env
											cJSON_DeleteItemFromObject(this_config, "Env");
											thisenv=cJSON_CreateArray();
											cJSON_AddItemToObject(this_config, "Env", thisenv);
										}
										merge_config(&v1_hist_config, this_config);
										cJSON_AddItemToObject(v1comp, "config", v1_hist_config);
										v1_hist_config=NULL;
									}
									else
									{
										cJSON_DeleteItemFromObject(v1comp, "config");
										cJSON* this_config=cJSON_CreateObject(); //cJSON_Parse( blank_config_str);
										cJSON_AddItemToObject(v1comp, "config", this_config);
									}
									
									char * v1comp_str_res=cJSON_PrintUnformatted(v1comp);
									if (v1comp_str_res)
									{
											cJSON_safe_addString2Obj(newhist, "v1Compatibility", v1comp_str_res);
											free(v1comp_str_res);
											v1comp_str_res=NULL;
									}
									cJSON_AddItemToArray(v1_manifest_history, newhist);
									newhist=NULL;
								}
								cJSON_Delete(v1comp);
								v1comp=NULL;
							
							}
							
							
							if (newhist) cJSON_Delete(newhist);
							newhist=NULL;
						}
								
					}//for hists
					if (parent)free(parent);
					parent=NULL;
				} // if hist
				
			}
			if (manifest) cJSON_Delete(manifest);
			manifest=NULL;
		} // schema version not a number!!
		else
		{
			debug("schemaVersion not a number\n");
			res=-1;
		}
		if (manifest) cJSON_Delete(manifest);
		manifest=NULL;
	} // if manifest
	else res=-1;
	return res;
}
/*
 {
	"architecture":	"amd64",
	"config":	{
 "Hostname":	"",
 "DomainName":	"",
 "User":	"",
 "WorkingDir":	"",
 "AttachStdin":	false,
 "AttachStdout":	false,
 "AttachStderr":	false,
 "Tty":	false,
 "OpenStdin":	false,
 "StdinOnce":	false,
 "Entrypoint":	null,
 "Volumes":	null,
 "OnBuild":	null,
 "Labels":	null,
 "ArgsEscaped":	true
	},
	"history":	[{
 "created_by":	"oc-runner",
 "created":	"2018-06-02T02:38:51.000000000Z"
 }],
	"created":	"2018-06-02T02:38:51.000000000Z",
	"os":	"linux",
	"rootfs":	{
 "type":	"layers",
 "diff_ids":	["sha256:2773f0b163458894f01fbe4661617add91c6da7de5d80cb3dc73866217f562e1"]
	}
 }
 */


/*
 "config":	{
 "Hostname":	"",
 "DomainName":	"",
 "User":	"",
 "WorkingDir":	"",
 "AttachStdin":	false,
 "AttachStdout":	false,
 "AttachStderr":	false,
 "Tty":	false,
 "OpenStdin":	false,
 "StdinOnce":	false,
 "Entrypoint":	null,
 "Volumes":	null,
 "OnBuild":	null,
 "Labels":	null,
 "ArgsEscaped":	true
	}
 */
//todo what's deifference of null and []
// todo more study needed on config
// todo does PATH get's ovewriten or ammended
// TODO more study needed !!!!!!


int merge_config(cJSON**config,cJSON*this_config)
{
	if (!config) return -1;
	if (!*config)
	{
		
		*config=cJSON_Parse( blank_config_str);
	}
	cJSON*item=NULL;
	cJSON*this_item=NULL;
	
	cJSON* env=cJSON_GetObjectItem(*config, "Env");
	cJSON* this_env=cJSON_GetObjectItem(this_config, "Env");
	if (env && this_env)
	{
		
		cJSON_ArrayForEach(this_item, this_env)
		{
			int dupl=0;
			int itemNumber=0;
			cJSON_ArrayForEach(item, env)
			{
				if (strcmp(this_item->valuestring,item->valuestring)==0)
				{
					dupl=1;
					break;
				}
				itemNumber++;
			}
			if (!dupl)
			{
				cJSON_AddItemToArray(env, cJSON_Duplicate(this_item, 1));
			}
			else
			{
				cJSON * new_env=cJSON_Duplicate(this_item, 1);
				cJSON_ReplaceItemInArray(env, itemNumber, new_env);
			}
		}
	}
	return 0;
}

int produce_config(cJSON*layerdata,cJSON ** config)
{
	if (!layerdata || !config || *config)
	{
		debug ("missing vars\n");
		return -1;
	}
	
	*config=cJSON_CreateObject();
	cJSON_AddStringToObject(*config, "architecture","amd64");
    cJSON_AddStringToObject(*config, "docker_version",PACKAGE_STRING);
	
	cJSON_AddItemToObject(*config, "config", cJSON_Duplicate(cJSON_GetObjectItem(layerdata, "conf_config"),1));
	cJSON_AddItemToObject(*config, "history", cJSON_Duplicate(cJSON_GetObjectItem(layerdata, "conf_history"),1));
	
	cJSON_AddTimeStampToObject(*config, "created");
	
	cJSON_AddStringToObject(*config, "os","linux");
	cJSON * rootfs=cJSON_CreateObject();
	cJSON_AddItemToObject(*config, "rootfs",rootfs);
	
	cJSON_AddStringToObject(rootfs, "type","layers");
	
	cJSON *ids=cJSON_GetObjectItem(layerdata, "config_diff_ids");
	if (ids)
	{
		cJSON_AddItemToObject(rootfs, "diff_ids", cJSON_Duplicate(ids,1));
	}
	else debug("NO ids in produce config\n");
	
	return 0;
}

/*
 {
	"schemaVersion":	2,
	"mediaType":	"application/vnd.docker.distribution.manifest.v2+json",
	"config":	{
 "mediaType":	"application/vnd.docker.container.image.v1+json",
 "size":	638,
 "digest":	"sha256:21a3d444ce9423ec971eb62712f624a6608ff8edd4f918cf56a3e5e9864325bc"
	},
	"layers":	[{
 "mediaType":	"application/vnd.docker.image.rootfs.diff.tar.gzip",
 "size":	2464445,
 "digest":	"sha256:31a85c91d2792879a6b2199b5d9531480fd3b9c11835fc0a6c75589b4d71f84a"
 }]
 }
 */
int produce_manifest_V2(cJSON*layerdata,cJSON ** manifest,char *config_digest,int config_size)
{
	if (!layerdata || !manifest || *manifest || !config_digest)
		return -1;
	*manifest=cJSON_CreateObject();
	cJSON_AddNumberToObject(*manifest, "schemaVersion",2);
	cJSON_AddStringToObject(*manifest,"mediaType","application/vnd.docker.distribution.manifest.v2+json");
	cJSON * config=cJSON_CreateObject();
	cJSON_AddStringToObject(config,"mediaType","application/vnd.docker.container.image.v1+json");
	cJSON_AddNumberToObject(config,"size",config_size);
	cJSON_AddStringToObject(config,"digest",config_digest);
	cJSON_AddItemToObject(*manifest, "config", config);
	cJSON_AddItemToObject(*manifest,"layers",cJSON_Duplicate(cJSON_GetObjectItem(layerdata, "layers"),1));
	return 0;
}

/*
 {
 "name": "hello-world",
 "tag": "latest",
 "architecture": "amd64",
 "fsLayers": [
 {
 "blobSum": "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef"
 },
 {
 "blobSum": "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef"
 },
 {
 "blobSum": "sha256:cc8567d70002e957612902a8e985ea129d831ebe04057d88fb644857caa45d11"
 },
 {
 "blobSum": "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef"
 }
 ],
 "history": [
 {
 "v1Compatibility": "{\"id\":\"e45a5af57b00862e5ef5782a9925979a02ba2b12dff832fd0991335f4a11e5c5\",\"parent\":\"31cbccb51277105ba3ae35ce33c22b69c9e3f1002e76e4c736a2e8ebff9d7b5d\",\"created\":\"2014-12-31T22:57:59.178729048Z\",\"container\":\"27b45f8fb11795b52e9605b686159729b0d9ca92f76d40fb4f05a62e19c46b4f\",\"container_config\":{\"Hostname\":\"8ce6509d66e2\",\"Domainname\":\"\",\"User\":\"\",\"Memory\":0,\"MemorySwap\":0,\"CpuShares\":0,\"Cpuset\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"PortSpecs\":null,\"ExposedPorts\":null,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"],\"Cmd\":[\"/bin/sh\",\"-c\",\"#(nop) CMD [/hello]\"],\"Image\":\"31cbccb51277105ba3ae35ce33c22b69c9e3f1002e76e4c736a2e8ebff9d7b5d\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"NetworkDisabled\":false,\"MacAddress\":\"\",\"OnBuild\":[],\"SecurityOpt\":null,\"Labels\":null},\"docker_version\":\"1.4.1\",\"config\":{\"Hostname\":\"8ce6509d66e2\",\"Domainname\":\"\",\"User\":\"\",\"Memory\":0,\"MemorySwap\":0,\"CpuShares\":0,\"Cpuset\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"PortSpecs\":null,\"ExposedPorts\":null,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"],\"Cmd\":[\"/hello\"],\"Image\":\"31cbccb51277105ba3ae35ce33c22b69c9e3f1002e76e4c736a2e8ebff9d7b5d\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"NetworkDisabled\":false,\"MacAddress\":\"\",\"OnBuild\":[],\"SecurityOpt\":null,\"Labels\":null},\"architecture\":\"amd64\",\"os\":\"linux\",\"Size\":0}\n"
 },
 {
 "v1Compatibility": "{\"id\":\"e45a5af57b00862e5ef5782a9925979a02ba2b12dff832fd0991335f4a11e5c5\",\"parent\":\"31cbccb51277105ba3ae35ce33c22b69c9e3f1002e76e4c736a2e8ebff9d7b5d\",\"created\":\"2014-12-31T22:57:59.178729048Z\",\"container\":\"27b45f8fb11795b52e9605b686159729b0d9ca92f76d40fb4f05a62e19c46b4f\",\"container_config\":{\"Hostname\":\"8ce6509d66e2\",\"Domainname\":\"\",\"User\":\"\",\"Memory\":0,\"MemorySwap\":0,\"CpuShares\":0,\"Cpuset\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"PortSpecs\":null,\"ExposedPorts\":null,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"],\"Cmd\":[\"/bin/sh\",\"-c\",\"#(nop) CMD [/hello]\"],\"Image\":\"31cbccb51277105ba3ae35ce33c22b69c9e3f1002e76e4c736a2e8ebff9d7b5d\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"NetworkDisabled\":false,\"MacAddress\":\"\",\"OnBuild\":[],\"SecurityOpt\":null,\"Labels\":null},\"docker_version\":\"1.4.1\",\"config\":{\"Hostname\":\"8ce6509d66e2\",\"Domainname\":\"\",\"User\":\"\",\"Memory\":0,\"MemorySwap\":0,\"CpuShares\":0,\"Cpuset\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"PortSpecs\":null,\"ExposedPorts\":null,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"],\"Cmd\":[\"/hello\"],\"Image\":\"31cbccb51277105ba3ae35ce33c22b69c9e3f1002e76e4c736a2e8ebff9d7b5d\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"NetworkDisabled\":false,\"MacAddress\":\"\",\"OnBuild\":[],\"SecurityOpt\":null,\"Labels\":null},\"architecture\":\"amd64\",\"os\":\"linux\",\"Size\":0}\n"
 },
 ],
 "schemaVersion": 1,
 "signatures": [
 {
 "header": {
 "jwk": {
 "crv": "P-256",
 "kid": "OD6I:6DRK:JXEJ:KBM4:255X:NSAA:MUSF:E4VM:ZI6W:CUN2:L4Z6:LSF4",
 "kty": "EC",
 "x": "3gAwX48IQ5oaYQAYSxor6rYYc_6yjuLCjtQ9LUakg4A",
 "y": "t72ge6kIA1XOjqjVoEOiPPAURltJFBMGDSQvEGVB010"
 },
 "alg": "ES256"
 },
 "signature": "XREm0L8WNn27Ga_iE_vRnTxVMhhYY0Zst_FfkKopg6gWSoTOZTuW4rK0fg_IqnKkEKlbD83tD46LKEGi5aIVFg",
 "protected": "eyJmb3JtYXRMZW5ndGgiOjY2MjgsImZvcm1hdFRhaWwiOiJDbjAiLCJ0aW1lIjoiMjAxNS0wNC0wOFQxODo1Mjo1OVoifQ"
 }
 ]
 }

 */

int produce_manifest_V1(cJSON*layerdata,cJSON ** manifest,char * name,char*tag)
{
	if (!layerdata || !manifest || *manifest || !name)
		return -1;
	*manifest=cJSON_CreateObject();
	cJSON_AddNumberToObject(*manifest, "schemaVersion",1);
	cJSON_AddStringToObject(*manifest, "architecture","amd64");
	cJSON_AddStringToObject(*manifest, "name",name);
	if (!tag)
		cJSON_AddStringToObject(*manifest, "tag","latest");
	else
		cJSON_AddStringToObject(*manifest, "tag",tag);
	
	cJSON * fslayers_source=cJSON_GetObjectItem(layerdata, "fsLayers");
	if (fslayers_source)
	{
		cJSON * fslayers=cJSON_CreateArray();
        if (fslayers)
		{
			cJSON * layer=NULL;
			cJSON_ArrayForEach(layer, fslayers_source)
			{
				cJSON* newlayer=cJSON_Duplicate(layer,1);
				if (newlayer)
					cJSON_AddItemToBeginArray(fslayers,newlayer);
			}
		cJSON_AddItemToObject(*manifest,"fsLayers",fslayers);
		}
	}
	cJSON * hist_source=cJSON_GetObjectItem(layerdata, "history");
	cJSON * hists=cJSON_CreateArray();
	cJSON * hist=NULL;
	cJSON_ArrayForEach(hist, hist_source)
	{
		cJSON * new_hist=cJSON_Duplicate(hist,1);
		if (new_hist)
			cJSON_AddItemToBeginArray(hists,new_hist);
	}

	cJSON_AddItemToObject(*manifest,"history",hists);
	
	return 0;
}






int calc_thresshold_files(const cJSON * threshold_path_obj, cJSON* job,char ** errormsg)
{
	int end_value=0;
	char * threshold_path=NULL;
	if ( threshold_path_obj->valuestring)
	{
		threshold_path=threshold_path_obj->valuestring;
		end_value=cache_calc_numberoffiles(threshold_path, job,errormsg);
		
	}
	else if (cJSON_IsArray( threshold_path_obj))
	{
		cJSON * item=NULL;
		cJSON_ArrayForEach(item, threshold_path_obj)
		{
			if (cJSON_IsString( item))
			{
				const char* threshold_path=item->valuestring;
				end_value=end_value+cache_calc_numberoffiles(threshold_path, job,errormsg);
			}
			else
				if (errormsg && !*errormsg)
					asprintf(errormsg, "threshold::path should be a an Array of Strings\n");
		}
	}
	else
		if (errormsg && !*errormsg)
			asprintf(errormsg, "threshold::path should be a string or an Array of Strings\n");
	return end_value;
}


int cache_calc_numberoffiles(const char * threshold_path, cJSON* job,char ** errormsg)
{
	int end_value=0;
	word_exp_var_t wev;
	cJSON * env_vars=cJSON_GetObjectItem(job, "env_vars");
	const char * CI_PROJECT_DIR=cJSON_get_key(env_vars, "env_vars");
	memset(&wev, 0, sizeof(word_exp_var_t));
	int res_w=word_exp_var_dyn(&wev, threshold_path , env_vars, CI_PROJECT_DIR);
	
	if (!res_w)
	{
		if (wev.argc>1)
		{
			end_value= (int)wev.argc;
			
			if (getdebug())
			{
				int i=0;
				debug("WS: %s ",threshold_path);
				for(i=0;i<wev.argc;i++)
				{
					debug("%d: %s\n",i,wev.argv[i]);
				}
			}
		}
		else if (wev.argc==1) // chek if exist otherwise we just have path
		{
			if (0 == access(wev.argv[0], 0)) {
				end_value= 1;
			}
			else {
				end_value= 0;
			}
		}
		else
			end_value= 0;
	}
	else if (errormsg && !*errormsg)
			asprintf(errormsg, "threshold_path %s:\n\t%s\n",threshold_path,wev.errmsg);

	word_exp_var_free(&wev);
	return end_value;
}

int cache_ws_check_push(cJSON * job,cJSON * item,char ** errormsg)
{
	int skip_push=0;
	cJSON * threshold=cJSON_GetObjectItem(item, "threshold");
	print_json(threshold);
	if(threshold)
	{
		const char *methode=cJSON_get_key(threshold, "method");
		cJSON * threshold_path_obj = cJSON_GetObjectItem(threshold, "path");
		if (threshold_path_obj)
		{
			if (!methode || ( methode && strcmp(methode,"file_count")==0))
			{
				int start_value=cJSON_safe_GetNumber(threshold, "initial_value");
				int end_value=0;
				if (start_value !=INT32_MAX)
				{
					
					end_value= calc_thresshold_files(threshold_path_obj,job,errormsg);
					
					if (end_value)
					{
						cJSON * operand=cJSON_GetObjectItem(threshold, "operand");
						if ((operand && cJSON_IsString(operand) && strcmp(operand->valuestring, "ne")==0) || !operand)
						{
							
							skip_push=(start_value==end_value);
							debug("skip Push: (ne) start: %d end: %d skip=%d\n",start_value,end_value,skip_push);
						}
						else if (operand && cJSON_IsNumber(operand))
						{
							// diff greater or equal then operand number
							skip_push=(end_value-start_value)<operand->valueint;
							debug("skip Push: (%d) start: %d end: %d skip=%d\n",operand->valueint,start_value,end_value,skip_push);
						}
						else
						{
							skip_push=0;
							// unknown operand
							debug("skip Push: unkown operand\n");
						}
						
					}
					else
					{
						skip_push=0;
						debug("skip Push: no end Value start: %d\n",start_value);
					}
				}
				else
				{
					// no initial value, push
					skip_push=0;
				}
			}
			else
			{
				skip_push=0;
				debug("skip Push: not implemented method for push threshold\n");
			}
		}
		else
		{
			debug("No path in thresshold\n");
		}
	}
	return skip_push;
}


int cache_process_path_obj(cJSON * job,cJSON *cache_path,word_exp_var_t*wev,const char*CI_PROJECT_DIR, char **errormsg)
{
	int result=0;
	if (!cache_path) return 0;
	cJSON*env_vars=cJSON_GetObjectItem(job, "env_vars");
	dynbuffer *b=dynbuffer_init();
	
	if (cache_path->valuestring)
	{
		debug("cachepath is string\n");
		const char*tmp_path=NULL;
		tmp_path=cache_path->valuestring;
		if (tmp_path && tmp_path[0]!='/')
			dynbuffer_write_v(b, "%s/%s",CI_PROJECT_DIR,tmp_path);
		else if (tmp_path && tmp_path[0]=='/')
				dynbuffer_write_v(b, "%s",tmp_path);
	}
	else if (cJSON_IsArray( cache_path))
	{
		debug("cachepath is array\n");
		cJSON * item=NULL;
		cJSON_ArrayForEach(item, cache_path)
		{
			if (cJSON_IsString( item))
			{
				
				const char*tmp_path=NULL;
				tmp_path=item->valuestring;
				debug("cachepath[item] %s\n",tmp_path);
				if (tmp_path && tmp_path[0]!='/')
					dynbuffer_write_v(b, "%s/%s ",CI_PROJECT_DIR,tmp_path);
				else if (tmp_path && tmp_path[0]=='/')
					dynbuffer_write_v(b, "%s ",tmp_path);
			}
			else
				if (errormsg && !*errormsg)
					asprintf(errormsg, "path not an Array of Strings\n");
		}
	}
	else
		if (errormsg && !*errormsg)
			asprintf(errormsg, "threshold::path should be a string or an Array of Strings\n");
	
	char *cache_path_str=NULL;
	dynbuffer_pop(&b, &cache_path_str, NULL);
	debug("cachepath string=\n%s\n",cache_path_str);
	if (cache_path_str)
	{
	    result=word_exp_var_dyn(wev, cache_path_str , env_vars, "/");
		free(cache_path_str);
		if (wev->errmsg) debug("wev error msg: %s\n",wev->errmsg);
		if (errormsg && !*errormsg && wev->errmsg)
		{
			*errormsg=wev->errmsg;
			wev->errmsg=NULL;
		}
		if (wev->argc>0)
		{
			int i=0;
			for (i=0;i<wev->argc;i++) debug("wev[i]=%s\n",wev->argv[i]);
		}
		else
		{
			debug("no wev items\n");
		}
	}
	return result;
}

void cache_do_clean(cJSON * job, struct trace_Struct *trace,cJSON * localShell)
{
	cJSON * layers_clean=NULL;
	layers_clean=cJSON_GetObjectItem(job, "layers_clean");
	if (layers_clean)
	{
		
		debug("Clean Layers\n");
		cJSON * item=NULL;
		struct oc_api_data_s *oc_api_data=NULL;
		const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
		const char * token= cJSON_get_key(sys_vars,"OC-RUNNER-TOKEN");
		const char * master_url= cJSON_get_key(sys_vars,"OC-MASTER-URL");
		
		if (!localShell) oc_api_init(&oc_api_data, DEFAULT_CA_BUNDLE, master_url, token);
		
		cJSON_ArrayForEach(item, layers_clean)
		{
			debug("Clean item\n");
			print_json(item);
			const char * name=cJSON_get_key(item, "name");
			const char * ocapi=cJSON_get_key(item, "ocapi");
			const char * location=cJSON_get_key(item, "location");
			if(name && strcmp(name,"git_cache")==0)
				Write_dyn_trace_pad(trace,yellow,55,"\n  * Clean %s ",name);
			else
				Write_dyn_trace_pad(trace,yellow,55,"\n  * Clean work_space %s ",name);
			debug("Clean %s\n",name);
			update_details(trace);
			if(ocapi)
			{
				int res=0;
				cJSON * result=NULL;
				debug ("delete_imagestreamtags %s\n",ocapi);
				res=oc_api(oc_api_data, "DELETE", NULL, &result,"%s", ocapi);
				if (!res)
				{
					Write_dyn_trace(trace,green,"       [OK]\n");
				}
				else if (res==404)
				{
					Write_dyn_trace(trace,yellow,"[not Found]\n");
				}
				else
				{
					Write_dyn_trace(trace,red,   "     [FAIL]\n");
					if (result)
					{
						Write_dyn_trace(trace, magenta,"\n   Status  :%s\n",cJSON_get_key(result, "status"));
						Write_dyn_trace(trace, magenta,  "   Reason  :%s\n",cJSON_get_key(result, "reason"));
						Write_dyn_trace(trace, magenta,  "   Message :%s\n",cJSON_get_key(result, "message"));
					}
					else
						print_api_error(trace, res);
					
				}
				if (result)
				{
					debug("\nResult ImageStreamTag Delete: %d\n",res);
					print_json(result);
					cJSON_Delete(result);
					result=NULL;
				}
			}
			else if(location)
			{   //clean dir
				vrmdir(location);
				vmkdir(location);
				Write_dyn_trace(trace,green," [OK]\n");
			}
			update_details(trace);
		}
		if (oc_api_data)oc_api_cleanup(&oc_api_data);
	}
	return ;
}

int cache_do_push(cJSON * job,struct trace_Struct * trace, cJSON* localShell)
{
	cJSON * cache_push=NULL;
	cache_push=cJSON_GetObjectItem(job, "layers_push");
	if (cache_push)
	{
		int res=0;
		debug("Push layers\n");
		cJSON * item=NULL;
		struct oc_api_data_s *oc_api_data=NULL;
		const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
        const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
        const char * CI_PROJECT_DIR=cJSON_get_key(env_vars, "CI_PROJECT_DIR");
		const char * token= cJSON_get_key(sys_vars,"OC-RUNNER-TOKEN");
		const char * master_url= cJSON_get_key(sys_vars,"OC-MASTER-URL");
		if (!localShell) oc_api_init(&oc_api_data, DEFAULT_CA_BUNDLE, master_url, token);
		debug("Loop cache_push \n");
		cJSON_ArrayForEach(item, cache_push)
		{
			print_json(item);
			cJSON  * cache_path=cJSON_GetObjectItem(item, "path");
			const char * cache_image=cJSON_get_key(item, "image");
			const char * cache_location=cJSON_get_key(item, "location");
			const char * cache_name=cJSON_get_key(item, "name");
			const char * cache_image_name=cJSON_get_key(item, "image_name");
			cJSON * wp_env=cJSON_GetObjectItem(item, "environment");
			if (!wp_env && !cache_path  && strcmp(cache_name,"git_cache")!=0)
			{
				Write_dyn_trace(trace,yellow,"\n  * Skip push ");
				Write_dyn_trace(trace,none,"%s\n",cache_name);
				Write_dyn_trace(trace,magenta,"\tWarning: no path nor environment defined for WORK_SPACE:push:path\n");
				continue;
			}
			debug("Push Item\n");
			print_json(item);
			if (localShell && cache_location && cache_path && strcmp(cache_name,"git_cache")!=0)
			{
				//Write_dyn_trace(trace,yellow,"\n  * copy %s\n",cache_name);
				//update_details(trace);
				
				// expand and copy cache utils.c
				
				Write_dyn_trace(trace,yellow,"\n  * copy %s : NOT IMPLEMENTED YET\n",cache_name);
				update_details(trace);
				
			}
			else if (!localShell)
			{
				int skip_push=1;
				int updated_git_objects=cJSON_safe_GetNumber(item, "remote_updates");
				if (updated_git_objects<INT32_MAX) debug("git updated objects=%d\n",updated_git_objects);
				if ((strcmp(cache_name, "git_cache")==0 && updated_git_objects<INT32_MAX && updated_git_objects>5) )
				{
					// gitcache and more then 5 objects, push
					skip_push=0;
				}
				else if( strcmp(cache_name, "git_cache")!=0 && (wp_env) )
				{
					// workspace with environment: push
					skip_push=0;
				}
				else if( strcmp(cache_name, "git_cache")!=0 )
				{
					char * errormsg=NULL;
					skip_push=cache_ws_check_push(job,item,&errormsg);
					if (errormsg)
					{
						Write_dyn_trace(trace,magenta,"\n  Warning: calcuating files for workspace : %s\n",cache_name);
						Write_dyn_trace(trace,none," \t%s \n",errormsg);
					}
				}
				if(!skip_push && cache_image)
				{
					debug("Start Push %s\n",cache_name);
					char * dest=NULL;
					if(cache_name && strcmp(cache_name,"git_cache")==0)
					{
						Write_dyn_trace(trace,yellow,"\n  * push %s \n",cache_name);
					}
					else
						Write_dyn_trace(trace,yellow,"\n  * push work_space %s \n",cache_name);
					debug("PUSH %s\n",cache_name);
					update_details(trace);
					create_imagestream(oc_api_data, job, cache_image_name);
					
					word_exp_var_t wev;
					memset(&wev, 0, sizeof(word_exp_var_t));
					char * errormsg=NULL;
					int res_cp=cache_process_path_obj(job,cache_path,&wev,CI_PROJECT_DIR,&errormsg);
					
					if (errormsg && res_cp>=0)
					{
						Write_dyn_trace(trace,magenta,"\n  Warning expanding %s:\n",cache_name);
						Write_dyn_trace(trace,none," \t%s \n",errormsg);
					}
					if (!res_cp && wev.argc>0)
					{
						debug("Expansion success : %d item(s)\n",wev.argc);
						res=int_registry_push(job,trace,"/",wev.argv,wev.argc,cache_image,NULL,wp_env);
					}
					else if (res_cp<0)
					{
						Write_dyn_trace(trace,magenta,"\n  Error expanding expanding %s:\n",cache_name);
						Write_dyn_trace(trace,none," \t%s \n",errormsg);
					}
					else if (wev.argc==0 && res_cp==0)
					{
						debug("wev.argc=0, pushing empty layer\n");
						res=int_registry_push(job,trace,NULL,NULL,0,cache_image,NULL,wp_env);
					}
					word_exp_var_free(&wev);
					free(dest);
					dest=NULL;
					
				} else
				{
					Write_dyn_trace(trace,yellow,"\n  * Skip push %s:\n",cache_name);
					debug("SKIP PUSH %s\n",cache_name);
				}
			}
			
		} // for each work_space
		
		if (oc_api_data)oc_api_cleanup(&oc_api_data);
		if (res)
		{
			Write_dyn_trace(trace,yellow,"\n\tERROR pushing cache, Non Fatal, continue\n");
		}
	}
	// Clean it all up
	return 0;
}

void cache_update_initial_filecount(cJSON * job,struct trace_Struct * trace,cJSON * localShell)
{
    cJSON * layers_push=cJSON_GetObjectItem(job, "layers_push");
    if(!localShell&& layers_push)
    {
        debug("layers_push check threshold\n");
        print_json(layers_push);
        cJSON * push=NULL;
        cJSON_ArrayForEach(push, layers_push)
        {
            cJSON * threshold=cJSON_GetObjectItem(push, "threshold");
            const char * wp_name=cJSON_get_key(push, "name");
            if (threshold)
            {
                const char *methode=cJSON_get_key(threshold, "method");
                const cJSON *threshold_path_obj=cJSON_GetObjectItem(threshold, "path");
                if ((!methode || ( methode && strcmp(methode,"file_count")==0)) && threshold_path_obj)
                {
                    char * errormsg=NULL;
                    cJSON_safe_addNumber2Obj(threshold, "initial_value", calc_thresshold_files(threshold_path_obj,job,&errormsg));
                    if (errormsg)
                    {
                        Write_dyn_trace(trace,magenta,"\n  Warning: calcuating files for workspace : %s\n",wp_name);
                        Write_dyn_trace(trace,none," \t%s \n",errormsg);
                    }
                }
            }
            
        }
        debug("layers_push check threshold Result\n");
        print_json(layers_push);
    }
    return;
}

void cache_update_remote_objects(cJSON*job,int remote_objects_update)
{
    if (!job || ! remote_objects_update) return;
    cJSON * layers_push=cJSON_GetObjectItem(job, "layers_push");
    cJSON * item=NULL;
    if (!layers_push) return;
    cJSON_ArrayForEach(item, layers_push)
    {
        const char * name=cJSON_get_key(item, "name");
        if (name && strcmp(name,"git_cache")==0)
        {
            cJSON_safe_addNumber2Obj(item, "remote_updates", remote_objects_update);
            break;
        }
    }
}

// for scope group
char * get_gitlab_group_url(const cJSON * env_vars)
{
	
	const char * ci_project_url=cJSON_get_key(env_vars,"CI_PROJECT_URL");
	const char * ci_project_name=cJSON_get_key(env_vars,"CI_PROJECT_NAME");
	const char *pos=strstr(ci_project_url,ci_project_name);
	if(pos)
	{
		size_t len=pos-ci_project_url-1;
		char * result =calloc(1, len+1);
		if (result)
			memcpy(result,ci_project_url,len);
		return result;
	}
	else
		return NULL;
}
