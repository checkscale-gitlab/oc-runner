//
//  specials.c
//  oc-runner
//
//  Created by Danny Goossen on 16/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"
#include "specials.h"
#include "wordexp_var.h"

/*------------------------------------------------------------------------
 * slug a null or non Null terminated string:
 * replace all non (a-zA-Z0-9) with -
 * no - at beginning or end, no subsequent dashes
 *
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * slug_it(const char * input_str, size_t len)
{
	 if (!input_str) return NULL;
   regex_t regex;
   int reti;
   size_t prev_end=0;
   char * output=NULL;
   char * instr=NULL;
   char regex_str[]= "[^a-zA-Z0-9]";
   int regex_match_cnt=1+1;

   regmatch_t   ovector[2];
   //regmatch_t  * ovector= calloc(sizeof(regmatch_t),regex_match_cnt);



   // prepare input and output buffer
   if (len)
   {
      output=calloc(1, len+1);
      instr=calloc(1, len+1);
      memcpy(instr, input_str, len);
   }
   else
   {
      len=strlen(input_str);
      output=calloc(1, strlen(input_str)+1);
      instr=strdup(input_str);
   }
	// should not happen!
	if (!output || !instr){ error("Out of memory\n");if (output) free(output);if (instr) free(instr);return NULL;}
   /* Compile regular expression */

   reti = regcomp(&regex, regex_str, REG_EXTENDED);
	// should not happen, regex is fixed, not variable
   if (reti) { error("Could not compile regex\n");regfree(&regex);if (instr) free(instr);if (output) free(output);return NULL;}

   /* Execute regular expression in loop */

   for (;;)
   {
      size_t len_subject;
      reti = regexec(&regex, instr+prev_end, regex_match_cnt, ovector, 0);
      if (!reti)
      {
         if (ovector[0].rm_so !=-1 && (ovector[0].rm_eo-ovector[0].rm_so) == 1)
         {
            if (!prev_end )
               memcpy(output,instr,ovector[0].rm_so);
            else
               memcpy(output+strlen(output),instr+prev_end,ovector[0].rm_so);

            len_subject=strlen(output);
            if ( len_subject>0 && output[len_subject-1]!='-' ) output[len_subject]='-';
            prev_end=prev_end+ovector[0].rm_eo;
         }
         else break;
      }
      else if (reti == REG_NOMATCH)
      {
         // copy rest from lastmatch end to end of subject
         memcpy(output+strlen(output),(instr+ prev_end), len-prev_end);
         break;
      }
      else
      { char errmsg[1024];regerror(reti, &regex,(char *) errmsg, 1024);error( "Regex match failed: %s\n", errmsg);break;}
   }

   //remove trailing -
   while(strlen(output) && output[strlen(output)-1]=='-') output[strlen(output)-1]='\0';

   regfree(&regex);
   if (strlen(output)==0)
   {
      if (output) free(output);
      if (instr) free(instr);
      return NULL;
   }
   if (instr) free(instr);
   // we only want lowercase
   lower_string(output);
   // chop anything longer then 63
   if (strlen(output)>63) output[63]=0;
   // we cant end with -
   if (len>61 && output[62]=='-') output[62]=0;
   return output;
}
char * resolve_const_vars(const char * s,const cJSON * env_vars)
{
	if (!s) return NULL;
	char * res=NULL;
	char we_result[1024];
	int e_result=word_exp_var(we_result, 1024, s, env_vars);
	if (e_result)
	{
		debug("error resolve image\n");
	}
	else
	{
		res=strdup(we_result);
		debug("Resolved variable %s to %s\n", s,res);
	}
	return res;
}

int resolve_vars(char ** image,const cJSON * env_vars);

int resolve_vars(char ** image,const cJSON * env_vars)
{
	if(!image || !*image) return -1;
	int res=0;
	char image_result[1024];
	int e_result=word_exp_var(image_result, 1024, *image, env_vars);
	if (e_result)
	{
		res=-1;
		free(*image);
		debug("error resolve image\n");
		*image=NULL;
	}
	else
	{
		free(*image);
		*image=strdup(image_result);
		debug("Resolved image %s\n",*image);
	}
	return res;
}

int change_image_tag(char ** image, const char *new_tag)
{
	if (!image || !*image || strlen(*image)==0) return -1;
	char *p_ddot=strchr(*image,':');
	char *p_at=strchr(*image,'@');
	char *p_slash=strchr(*image,'/');

	char * start_ref=NULL;
    if (image && *image && strlen(*image)>0 )
	{
		char * tag_sep=p_ddot;
		if (p_at) tag_sep=p_at;
		if (tag_sep)
		{
			char *p_ddot2=strchr(p_ddot+1,':');
			if (p_at)
			{
				start_ref=p_at;
			}
			else if (p_ddot2)
			{
				start_ref=p_ddot2;
			}
			else if (p_slash && p_ddot >p_slash)
			{
				start_ref=p_ddot;
			}
			else if (!p_slash)
				start_ref=p_ddot;
		}
	}
	char* new_image=NULL;
	if (start_ref)
	{
		if (new_tag && strlen(new_tag)>0)
		{
			size_t len_2ddot=start_ref-(*image);
			new_image=calloc(1,(start_ref-(*image))+1+strlen(new_tag)+1 );
			if (new_image)
			sprintf(new_image,"%.*s:%s",(int)len_2ddot,*image,new_tag );
		}
		else
		{
			size_t len_2tag_sep=start_ref-(*image);
			new_image=calloc(1,(start_ref-(*image))+1 );
			if (new_image)
				sprintf(new_image,"%.*s",(int)len_2tag_sep,*image );
		}

	}
	else
	{
		if (new_tag && strlen(new_tag)>0)
		{
			new_image=calloc(1,(strlen(*image))+1+strlen(new_tag)+1 );
			if(new_image)
			sprintf(new_image,"%s:%s",*image,new_tag );
		}
		else
		{
			new_image=calloc(1,(strlen(*image))+1 );
			if (new_image)
				sprintf(new_image,"%s",*image );
		}

	}
	if (new_image)
	{
		free(*image);
		*image=new_image;
		return 0;
	}
	else
		return -1;

}

/*
int process_executor_image_nick(char ** image,const char * Imagestream_ip, const char * oc_name_space)
{
	if (!image || !*image || strlen(*image)==0) return -1;
	if (strstr(*image,"ImageStream")==*image)
	{
		// it's imageStream
		char image_check[1024];
		char * p=NULL;
		if (strstr(*image,"ImageStream/")==*image)
			p=(*image)+strlen("ImageStream/");
		else
			p=*(image)+strlen("ImageStream");

		if (strlen(p)==0)
			return -1;
		else
		{
			char *p_ddot_nick=strchr(p,':');
			char * slugged_nick=NULL;
			if (p_ddot_nick && p_ddot_nick != p) // slug till :
			{
				slugged_nick=slug_it(p, p_ddot_nick-p);
				if (slugged_nick) snprintf(image_check,1024 ,"%s/%s/is-%s%s",Imagestream_ip,oc_name_space,slugged_nick,p_ddot_nick);
			}
			else if (!p_ddot_nick)
			{
				if (slugged_nick)free(slugged_nick);
				return -1;
			}
			else
			{
				if (slugged_nick)free(slugged_nick);
				return -1;
			}
			if (slugged_nick)free(slugged_nick);
		}
		free(*image);
		*image=strdup(image_check);
	}
	return 0;
}

*/

char * process_image_nick(char ** image,const cJSON * env_vars,const char * Imagestream_ip, const char * oc_name_space,const char * docker_registry)
{
	if (!image || !*image || strlen(*image)==0) return NULL;
	char *result=NULL;

	resolve_image_var(image, env_vars);

	char *p_dot=strchr(*image,'.');
	char *p_ddot=strchr(*image,':');
	char *p_slash=strchr(*image,'/');


    if (strstr(*image,"ImageStream")==*image)
	{
		// it's imageStream
		char image_check[1024];
		char * p=NULL;
		if (strstr(*image,"ImageStream/")==*image)
			p=(*image)+strlen("ImageStream/");
		else
			p=*(image)+strlen("ImageStream");

		if (p && strlen(p)==0)
			snprintf(image_check,1024 ,"%s/%s/is-%s",Imagestream_ip,oc_name_space,cJSON_get_key(env_vars,"CI_PROJECT_PATH_SLUG"));
		else
		{
			char *p_ddot_nick=strchr(p,':');
			char *p_at_nick=strchr(p,'@');
			char * slugged_nick=NULL;
			char*tag_sep=p_ddot_nick;
			if (p_at_nick)
			{
				tag_sep=p_at_nick;

			}
			if (tag_sep && tag_sep != p ) // slug till :
			{

				slugged_nick=slug_it(p, tag_sep-p);
				if (slugged_nick) snprintf(image_check,1024 ,"%s/%s/is-%s-%s%s",Imagestream_ip,oc_name_space,cJSON_get_key(env_vars,"CI_PROJECT_PATH_SLUG"),slugged_nick,tag_sep);

			}
			else if (!tag_sep )
			{
				slugged_nick=slug_it(p, 0);

				 if (slugged_nick)
					 snprintf(image_check,1024 ,"%s/%s/is-%s-%s",Imagestream_ip,oc_name_space,cJSON_get_key(env_vars,"CI_PROJECT_PATH_SLUG"),slugged_nick);
			}
			else
				snprintf(image_check,1024 ,"%s/%s/is-%s%s",Imagestream_ip,oc_name_space,cJSON_get_key(env_vars,"CI_PROJECT_PATH_SLUG"),p);

			if (slugged_nick)free(slugged_nick);
		}


		result=strdup(image_check);

	}
	else if ( strstr(*image,"Gitlab")==*image)
	{
		// it's gitlab
		debug("image is Gitlab registry\n");
		if (!cJSON_get_key(env_vars,"CI_REGISTRY_IMAGE"))
		{
			debug("image is Gitlab, but no CI_REGISTRY_IMAGE in env \n");
			result=NULL; // not needed just for clarity
		}
		else
		{
			char image_check[1024];
			snprintf(image_check,1024 ,"%s%s",cJSON_get_key(env_vars,"CI_REGISTRY_IMAGE"),(*image)+strlen("Gitlab"));
			result=strdup(image_check);
		}
	}
	else
	{
		// check if docker
		if ((p_dot && !p_ddot) || (p_dot && p_ddot && (p_dot < p_ddot) ))
		{ // image contains a registry_domain
			result=strdup(*image);
		}
		else
		{   debug("image does not contain registry so prepend %s\n",docker_registry);
			if (docker_registry && *image)
			{
				size_t len_dock_regi=strlen(docker_registry);
				size_t len_image=strlen(*image);
				if ( !p_slash )
				{   // add library/ if no /
					debug("add library/\n");
					len_image=len_image+strlen("library/");
					result=calloc(1,len_dock_regi+1+len_image+1);
					sprintf(result,"%s/library/%s",docker_registry,*image);
				}
				else
				{
					result=calloc(1,len_dock_regi+1+len_image+1);
					sprintf(result,"%s/%s",docker_registry,*image);
				}

			}
			else return NULL;
		}
	}

	return result;
}

/*

// TODO remove this, use above function!!!
int prep_image(char ** image,const cJSON * env_vars, const char * ImageStream_domain , const char * oc_name_space)
{
	int res=0;
	res=resolve_image_var(image, env_vars);
	if (!res && image && *image && strstr(*image,"ImageStream/")==*image)
	{
		char image_check[1024];
		snprintf(image_check,1024 ,"%s/%s/%s",ImageStream_domain,oc_name_space,*image+strlen("ImageStream/"));
		free(*image);
		*image=strdup(image_check);
	}
	return res;
}
*/




int resolve_image_var(char ** image,const cJSON * env_vars)
{
   int res=0;
   int work=0;
   if (image && *image && strchr(*image, '$')!=NULL)
   {
      int cnt=0;
      do
      {
         cnt ++;
         char * p=NULL;
         if ((p=strchr(*image,':')))
         {
            // split it
            char * image_name=strdup(*image);
            image_name[p-(*image)]=0;
            resolve_vars(&image_name,env_vars);
            char * tag=strdup(p+1);
            res=resolve_vars(&tag,env_vars);
            free(*image);
            *image=NULL;
            if (image_name &&  tag)
            {
            *image=calloc(1,strlen(image_name)+1+strlen(tag)+1);
            //and put it back together
            sprintf(*image, "%s:%s",image_name,tag);
            }
            if (tag)free(tag);
            if(image_name)free(image_name);
            tag=NULL;
            image_name=NULL;
         }
         else
            res=resolve_vars(image,env_vars);

         if (*image)
            work=(strchr(*image,'$')!=NULL );
         else
            work=0;
      }
      while (work && cnt<5);
   }
   return res;
}


//TODO: add , int * IsDigest
int chop_image(const char *image,char ** registry_name,char **namespace,char **reference)
{
	if (!image) return -1;
	if (namespace && *namespace) free(*namespace);
	if(reference && *reference) free(*reference);
	if (registry_name && *registry_name) free(*registry_name);
	if(reference) *reference=NULL;
	if (namespace) *namespace=NULL;
	if (registry_name) *registry_name=NULL;

	const char * start=image;
	char *fs=strchr(image,'/');
	char *pt=strchr(image,'.');
	char *pdd=strchr(image,':');
	char * tmp_image_name=NULL;
	if (fs && pt && pt<fs)
	{
		// we got registry, move start
		start=fs+1; // name space should not start with /
		tmp_image_name=cJSON_strdup_n((const unsigned char*)image, fs-image);
	}
	else if (!pt || (pt && pdd && pt>pdd))
	{
		// no registry=> take default
		tmp_image_name=strdup(DOCKER_REGISTRY);
	}
	else if (!fs && pt)
	{
		tmp_image_name=strdup(image);
		start=NULL;
	}

	if (start)
	{
		char *dd=strchr(start,':');
		char *at=strchr(start,'@');
		if (dd && !at)
		{

			if (namespace) *namespace=cJSON_strdup_n((const unsigned char*)start, dd-start);

			if (reference) *reference=strdup(dd+1);
		}
		else if( at) // image refered with @DIGEST
		{
			if (namespace) *namespace=cJSON_strdup_n((const unsigned char*)start, at-start);

			if (reference) *reference=strdup(at+1);
		}
		else
		{
			if (namespace) *namespace=strdup(start);
		}
	}
	//TODO remove, image should be complete
	if ( tmp_image_name && strcmp(tmp_image_name,DOCKER_REGISTRY)==0)
	{
		if (namespace && *namespace &&!strchr(*namespace,'/'))
		{
			char * tmp=calloc(1,strlen(*namespace)+1+strlen("library/"));
			sprintf(tmp, "library/%s",*namespace);
			free(*namespace);
			*namespace=tmp;
		}
	}
	if (registry_name)
	{
		*registry_name=tmp_image_name;
		tmp_image_name=NULL;
	}
	if (tmp_image_name) free(tmp_image_name);
	return 0;
}

char * name_space_rev(const char * namespace)
{
	if (!namespace) return NULL;
	char* result=NULL;
	size_t len_ns=strlen(namespace)+1;
	size_t len_left=len_ns;
	char* temp=calloc(1, len_ns);
	char *p=NULL;
	char *pp=NULL;
	do
	{
		p=memrchr(namespace, '/',len_left);
		if (p||pp)
		{
			if ( strlen(temp)>0)
			{
				char * ns_1=NULL;
				if (p)
				{
					if (strncmp(temp, p+1, strlen(temp))!=0)
						asprintf(&ns_1,"%.*s",(int)(pp-p)-1,p+1);
				}
				else
				{
					if (strncmp(temp, namespace, strlen(temp))!=0)
					{
						int len=(int)(pp-namespace);
						asprintf(&ns_1,"%.*s",len,namespace);
					}
				}
				if (ns_1 && (strncmp("library", ns_1, strlen("library"))!=0))
				{
					char *r=NULL;
					asprintf(&r, "%s %s",temp,ns_1);
          if (temp) free(temp);
					free(ns_1);
					temp=r;
				}
				break;
			}
			else if(p)
			{ // name of namesapce
				snprintf(temp, len_ns, "%s",p+1);
				len_left=len_ns-strlen(temp)-2;
			}
			pp=p;
		}
	} while(p);
	if (temp && strlen(temp)>0)result=strdup(temp);
	if (temp) free(temp);
	return result;
}

char * name_space_rev_slug(const char * namespace)
{
	if (!namespace) return NULL;
	char* result=NULL;
  char * temp=name_space_rev(namespace);
	if (temp)
	{
		result=slug_it(temp, 0);
		free(temp);
	}
	return result;
}



void recursive_dir_extract(const char * file, size_t len, cJSON * list, void(*fn)(void * userp,char *path),void * userp )//struct archive *a_content,struct archive *a_compressed)
{
	const char * p=memrchr(file,'/', len);
	if (p)
	{
		size_t newlen=p-file;
		if (newlen>0)
		{
			int res=cJSON_findinstringarry_n(list, file, newlen);
			if (res!=0)
			{
				recursive_dir_extract(file,newlen,list,(fn),userp);
				cJSON* tmp=cJSON_CreateObject();
				tmp->type=cJSON_String;
				tmp->valuestring=strndup(file, newlen);
				fn(userp,tmp->valuestring); // callback action
				cJSON_AddItemToBeginArray(list, tmp);

			}
		}
	}
}



cJSON * comma_sep_list_2_json_array(const char * list)
{
	cJSON *result=NULL;
	char *input=NULL;
	
	if (list) input=strdup(list);
	else return NULL;
	
	char *token;
	char *state;
	
	for (token = strtok_r(input, ",", &state);
		 token != NULL;
		 token = strtok_r(NULL, ",", &state))
	{
		if (!result) result=cJSON_CreateArray();
		if (result)
		{
			while (strlen(token)>1 && token[strlen(token)-1]==' ') token[strlen(token)-1]='\0';
			size_t c=0;
			while (strlen(token+c)>1 && token[c]==' ') c++;
			cJSON_add_Array_string(result,token+c);
		}
		else break;
	}
	//print_json(result);
	return result;
}

int env_var_is_false(const char *name)
{
	int result=0;
	const char* cvar=getenv(name);
	char *var=NULL;
	if (cvar) var=strdup(cvar);
	if (var) lower_string(var);
	if (var)
	{
		if (strcmp(var, "false")==0 || strcmp(var, "no")==0 || strcmp(var, "off")==0 || strcmp(var, "0")==0 ) result=1;
		free(var);
	}
	return result;
}

int env_var_is_true(const char *name)
{
	int result=0;
	const char* cvar=getenv(name);
	char *var=NULL;
	if (cvar) var=strdup(cvar);
	if (var) lower_string(var);
	if (var)
	{
		if (strcmp(var, "true")==0 || strcmp(var, "yes")==0 || strcmp(var, "on")==0 || strcmp(var, "1")==0 ) result=1;
		free(var);
	}
	return result;
}
