/*
 cJSON_deploy.c
 Created by Danny Goossen, Gioxa Ltd on 11/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
/*! \cJSON_deploy.c
 *  \brief cJSON extensions
 *  \author Danny Goossen
 *  \date 11/2/17
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */
#include "deployd.h"

void cJSON_AddTimeStampToObject(cJSON * object,const char *key)
{
	if(!object || !key) return;
	struct tm *tm_gmt;
	time_t t;
	t = time(NULL);
	tm_gmt = gmtime(&t);
	char date_buffer[36];
	strftime(date_buffer, 36, "%Y-%m-%dT%H:%M:%S.000000000Z", tm_gmt);

	if (cJSON_GetObjectItem(object,key))
		cJSON_ReplaceItemInObject(object, key, cJSON_CreateString(date_buffer));
	else
		cJSON_AddStringToObject(object,key,date_buffer);
}


// dupplicate a string from a non null terminated string
char* cJSON_strdup_n(const unsigned char * str, size_t n)
{
   size_t len = 0;
 char *copy = NULL;

   if (str == NULL)
   {
      return NULL;
   }

   len = n + 1;
   if (!(copy = (char *)calloc(len,1)))
   {
      return NULL;
   }
   memcpy(copy, str, len);
   copy[n]=0;
   return copy;
}

// add item to object with a key derived from a non 0 terminated string
void   cJSON_AddItemToObject_n(cJSON *object, const char *string,size_t n , cJSON *item)
{
   if (!item)
   {
      return;
   }

   /* free old key and set new one */
   if (item->string)
   {
      free(item->string);
   }
   item->string = (char*)cJSON_strdup_n((const unsigned char*)string,n);
   cJSON_AddItemToArray(object,item);
}

// create a string object from a non null terminated string
cJSON *cJSON_CreateString_n(const char *string, size_t n)
{
   cJSON *item = cJSON_CreateObject();
   if(item)
   {
      item->type = cJSON_String;
      item->valuestring = (char*)cJSON_strdup_n((const unsigned char*)string, n);
      if(!item->valuestring)
      {
         cJSON_Delete(item);
         return NULL;
      }
   }

   return item;
}


/* insert item at the beginning of the array/object. */
void   cJSON_AddItemToBeginArray(cJSON *array, cJSON *item)
{
     if (!array) return;
    cJSON *c = array->child;
    if (!item)
    {
        return;
    }
    if (!c)
    {
        /* list is empty, start new one */
        array->child = item;
    }
    else
    {
        array->child=item;
        item->next=c;
        c->prev=item;
    }
}

// create an JSON tree from **envp
// but only for environment vars starting with : CI_, GITLAB_ or DEPLOY_
cJSON * cJSON_Create_env_obj(char** envp)
{
   cJSON *environment = cJSON_CreateObject();
   char** env;
   for (env = envp; *env != 0; env++)
   {
      char* thisEnv = *env;
      //printf("thisenv: %s\n", thisEnv);
      char * e=thisEnv;
      int len=(int)strlen(thisEnv);
      int pos = 0;
      while (*e++ != '=' && pos < len) pos++;
      //if (pos != len && pos >1)
      {
         //if (strncmp(thisEnv,"CI_",3)==0 || \
         //   ( pos >8 && strncmp(thisEnv,"DEPLOY_",7)==0 )|| \
         //    ( pos>7 && strncmp(thisEnv,"GITLAB",6)==0) ) \
		  
		{
           cJSON_AddItemToObject_n(environment, thisEnv, pos, cJSON_CreateString(thisEnv+pos+1));
         }
      }
   }
   return environment;
}


void cJSON_append_env_obj_from_dkr_Env(cJSON**target,const cJSON * dkr_env)
{
	if (!target || !dkr_env) return ;
	if (!*target) *target= cJSON_CreateObject();
	cJSON * item=NULL;
	cJSON_ArrayForEach(item, dkr_env)
	{
		const char* thisEnv = item->valuestring;
	    if (thisEnv && strlen(thisEnv)>0)
		{
			//printf("thisenv: %s\n", thisEnv);
			const char * e=thisEnv;
			int len=(int)strlen(thisEnv);
			int pos = 0;
			while (*e++ != '=' && pos < len) pos++;
			{
					cJSON_AddItemToObject_n(*target, thisEnv, pos, cJSON_CreateString(thisEnv+pos+1));
			}
		}
	}
	return;
}


// compaire a string value of a key, check if present and compaire
// returns 0 if match
int validate_key(const cJSON * item, const char * key,const char * value )
{
   if (!key || !item) return -2;
   if (item->type==cJSON_String && !value) return -1;
   const cJSON * tmp_json=cJSON_GetObjectItem(item, key );
   if (tmp_json)
   {
       if (tmp_json->valuestring && value)
          return(strcmp(value,tmp_json->valuestring)); // return 0 on equal string
       else if (!value && !tmp_json->valuestring)
          return 0; // NULL==NULL
       else
          return -1;
   }
   else
   {

     return(-1);
     //else return 0; // no item == no value
   }
}

// get a key valuestring but check if exist
// return NULL on non exist
int get_key(const cJSON * item, const char * key,char ** value )
{

   if (!key) return -2;
   cJSON * tmp_json=cJSON_GetObjectItem(item, key );
   if (tmp_json)
   {
      if (value) *value=tmp_json->valuestring;
      return (0);
   }
   else
   {
      return(-1);
   }
}

// parse the JSON env-tree to an **envp
int parse_env(cJSON * env_json, char *** newevpp)
{
   int i;
   size_t len;
   int array_size = cJSON_GetArraySize(env_json);
   *newevpp=calloc(sizeof(char *), array_size+1);
   char**walk=*newevpp;
   for (i = 0; i < array_size; i++)
   {
      cJSON *subitem = cJSON_GetArrayItem(env_json, i);
      if (cJSON_IsString(subitem))
      {
      len=strlen(subitem->string)+1+strlen(subitem->valuestring) +1;
      *walk=calloc(1, len);
      sprintf(*walk, "%s=%s",subitem->string,subitem->valuestring);
        // printf("%s\n",*walk);
      }
      walk++;
   }
   return (0);
}

// cleanup of env created with : int parse_env(cJSON * env_json, char *** newevpp)
void free_envp(char *** envp)
{
    char ** walk = *envp;
    while (*walk)
    {
        if (*walk) free(*walk);
        *walk=NULL;
        walk ++;
    }
    free(*envp);
    *envp=NULL;
}


// check if item is present in json string array
int in_string_array(const cJSON * json_list, char * search)
{
   if (!json_list || json_list->type==cJSON_NULL ||  json_list->type==cJSON_Invalid) return -3;
   if (!search) return 0;

   cJSON * pos=NULL;
   int found=0;
   cJSON_ArrayForEach(pos,json_list)
   {
      if (pos->valuestring && strcmp(search,pos->valuestring)==0)
      {
         found =1;
         break;
      }
   }
   return !found;
}

// check if list of keys is present in JSON structure
int check_presence(cJSON * env_json, char ** list, char ** error_msg)
{
   if (!env_json) return -1;
   if (!list) return 0;
   char** item;
   int result=0;
   for (item = list; *item != NULL; item++)
   {
      if(!cJSON_GetObjectItem(env_json, *item))
      {
         result++;
         if (error_msg&&*error_msg) asprintf(error_msg, "\nERROR: Missing \"%s\"\n",*item);
      }
   }
   return result;
}

// get's the object->valuestring of key
// return NULL on non exiting
// return a null terminated string,
// caller should not freeit, points to string in object
const char * cJSON_get_key(const cJSON * item, const char * key)
{
   if (!item || !key ) return NULL;
   const char * result=NULL;
   cJSON * thisitem=cJSON_GetObjectItem(item, key);
   if (thisitem) result=thisitem->valuestring;
   return result;
}
char * cJSON_get_stringvalue_as_var(const cJSON * item, const char * key)
{
	if (!item || !key ) return NULL;
	char * result=NULL;
	cJSON * thisitem=cJSON_GetObjectItem(item, key);
	if (thisitem) result=thisitem->valuestring;
	return result;
}

char * cJSON_get_stringvalue_dup(const cJSON * item, const char * key)
{
	if (!item || !key ) return NULL;
	char * result=NULL;
	cJSON * thisitem=cJSON_GetObjectItem(item, key);
	if (thisitem && thisitem->valuestring) result=strdup(thisitem->valuestring);
	return result;
}

int cJSON_safe_IsTrue(const cJSON *object,char * key)
{
	if (!object) return 0;
	cJSON * tp=cJSON_GetObjectItem(object,key );
	if (tp) return cJSON_IsTrue(tp);
	else return 0;
}

int cJSON_safe_IsFalse(const cJSON *object,char * key)
{
	if (!object) return 0;
	cJSON * tp=cJSON_GetObjectItem(object,key );
	if (tp) return cJSON_IsFalse(tp);
	else return 0;
}


int32_t cJSON_safe_GetNumber(const cJSON *object,char * key)
{
	if (!object)
		return INT32_MAX;
	cJSON * tp=cJSON_GetObjectItem(object,key );
	if (tp && cJSON_IsNumber(tp))
		return tp->valueint;
	else
		return INT32_MAX;
}




void cJSON_add_string(cJSON * target,const char * key,const char * value)
{
	if (value)
	{
		if (cJSON_GetObjectItem(target,key))
			cJSON_ReplaceItemInObject(target,key,cJSON_CreateString( value));
		else
			cJSON_AddStringToObject(target,key,value);
	}
	return;
}

void cJSON_add_string_n(cJSON * target,const char * key,const char * value,size_t len)
{
	if (value)
	{
		if (cJSON_GetObjectItem(target,key))
			cJSON_ReplaceItemInObject(target,key,cJSON_CreateString_n( value,len));
		else
			cJSON_AddStringToObject_n(target,key,value,len);
	}
	return;
}


void cJSON_add_string_v(cJSON * target,const char * key,const char * content,...)
{
	char * value=CSPRINTF(content);
	if (value)
	{
		if (cJSON_GetObjectItem(target,key))
			cJSON_ReplaceItemInObject(target,key,cJSON_CreateString( value));
		else
			cJSON_AddStringToObject(target,key,value);
		free(value);
	}
	return;
}

void cJSON_add_string_from_object(cJSON * target,const char * newkey,const cJSON * env_vars,const char * key)
{
	const char * value=cJSON_get_key(env_vars,key );
	if (value)
	{
		if (cJSON_GetObjectItem(target,newkey))
			cJSON_ReplaceItemInObject(target,newkey,cJSON_CreateString( value));
		else
			cJSON_AddStringToObject(target,newkey,value);
	}
	return;
}

void cJSON_add_Array_string(cJSON * target,const char * value)
{
	if (value)
	{
		cJSON_AddItemToArray(target, cJSON_CreateString(value));
	}
	return;
}


void cJSON_add_Array_string_v(cJSON * target,const char * string,...)
{
	char * value=CSPRINTF(string);
	if (value)
	{
		cJSON *item = cJSON_CreateObject();
		if(item)
		{
			item->type = cJSON_String;
			item->valuestring = value;
			cJSON_AddItemToArray(target, item);
		}
		else free(value);
	}
	return;
}

void cJSON_add_Array_string_n(cJSON * target,const char * string,size_t n)
{
	cJSON *item = cJSON_CreateObject();
	if(item)
	{
		item->type = cJSON_String;
		item->valuestring = (char*)cJSON_strdup_n((const unsigned char*)string, n);
		if(!item->valuestring)
		{
			cJSON_Delete(item);
			return;
		}
		else
		{
			cJSON_AddItemToArray(target, item);
		}
	}
	return;
}


int cJSON_findinstringarry_n(cJSON * sa,const char * content,size_t n)
{
	int res=-1;
	cJSON * item=NULL;
	cJSON_ArrayForEach(item, sa)
	{
		if (cJSON_IsString(item))
		{
			if (strncmp(item->valuestring, content, n)==0)
			{
				res=0;
				break;
			}
		}
		else
			break;
	}
	return res;
}

int cJSON_IsTrue_string(const char *valuestring)
{
	int result=0;
	if (*valuestring)
	{
		char * tmp=strdup(valuestring);
		lower_string(tmp);
		if(strcmp(tmp,"true")==0 || strcmp(tmp,"yes")==0 || strcmp(tmp,"1")==0)
			result=1;
		free(tmp);
		tmp=NULL;
	}
	return result;
}


int cJSON_validate_contains_match(const cJSON * json,const char *key,const char * match)
{
	int result = 0;
	if (!json) return 0;
	if (!key) return 0;
	if (!match) return 0;
	char * valuestring=cJSON_get_stringvalue_dup(json, key);
	if (!valuestring) return 0;
	lower_string(valuestring);
	char * match_ls=strdup(match);
	if (match)
	{
		lower_string(match_ls);
		result=strstr(valuestring,match_ls)!=NULL;
		free(match_ls);
	}
	free(valuestring);
	return result;
}

/*------------------------------------------------------------------------
 * Read a JSON file with dirpath/filename
 * returns NULL on errors
 * returns a cJSON object, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
cJSON * read_file_json_v( char ** errormsg,const char * vpath,...)
{
	if (!vpath || strlen(vpath)==0)
	{
		if (errormsg) asprintf(errormsg, "vpath is empty");
		return NULL;
	}
	char *dirpath=CSPRINTF(vpath);
	if (!dirpath || strlen(dirpath)==0)
	{
		if (errormsg) asprintf(errormsg, "Filename results empty");
		return NULL;
	}
	FILE *f = fopen(dirpath, "r");
	if (f == NULL)
	{
		if (errormsg) asprintf(errormsg, "Reading %s: %s",dirpath, strerror(errno));
		if (dirpath) free(dirpath);
		return NULL;
	}
	
	dynbuffer *mem= dynbuffer_init();
	char buf[1024];
	bzero(buf, 1024);
	while( fgets( buf, 1024-1, f ) != NULL )
	{
		dynbuffer_write(buf, &mem );
		bzero(buf, 1024);
	}
	fclose(f);
	cJSON * result=NULL;
	if (dynbuffer_get(mem) && dynbuffer_len(mem)>0)
	{
		result=cJSON_Parse(dynbuffer_get(mem));
		if (!result && errormsg) asprintf(errormsg, "file %s parsing returns NULL object", dirpath);
	}
	else
		if (errormsg) asprintf(errormsg, "file %s is empty", dirpath);
	dynbuffer_clear(mem);
	if (dirpath) free(dirpath);
	dirpath=0;
	return result;
}
