/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/**
 * \file curl_headers.h
 * \brief helper callback to retrieve curl headers into cJSON object on the fly
 * \author Created by Danny Goossen on  8/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 */


#ifndef __oc_runner__curl_headers__
#define __oc_runner__curl_headers__

#include <stdio.h>
#include <cJSON.h>

/**
 * \brief data struct for curl_headers_cb
*/
struct curl_headers_struct {
	cJSON * header; /**< cJSON object holding the http headers returned from a curl request */
};


/**
 * \brief free the cJSON header object
 */
void curl_headers_free( void *userp);

/**
 \brief actual callback function
 \returns size writen
 \code struct curl_headers_struct headers;
 curl_headers_init(&headers);
 curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION,curl_headers_cb);
 curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &headers_res);
 // execute request
 res = curl_easy_perform(curl);
 const char * header_x=cJSON_get_key(headers.header,"header_x");
 curl_headers_free(headers)
 */
size_t curl_headers_cb(void *contents, size_t size, size_t nmemb, void *userp);


/**
 * \brief creates an emty headero object
 */
void curl_headers_init( void *userp);



#endif /* defined(__oc_runner__curl_headers__) */
