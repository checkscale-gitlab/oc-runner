//
//  FileCopy.h
//  oc-runner
//
//  Created by Danny Goossen on 9/8/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __oc_runner__FileCopy__
#define __oc_runner__FileCopy__

#include <stdio.h>
#include "cJSON_deploy.h"

int FileCopy(const cJSON * job,struct trace_Struct * trace,size_t argc,char * const *argv);

#endif /* defined(__oc_runner__FileCopy__) */
