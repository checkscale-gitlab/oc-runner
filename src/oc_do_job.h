/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file oc_do_job.h
 *  \brief header for dispatcher job handler
 *  \author Danny Goossen
 *  \date 9/10/17
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#ifndef __deployctl__oc_do_job__
#define __deployctl__oc_do_job__

#include "common.h"

/*! \fn int do_job_oc(cJSON * runner,cJSON* job,parameter_t * parameters )
 *  \brief spin a pod to execute the job
 *  This is the main function of the dispatcher.
 *  It checks the image to use as defined by the job or default
 *  It prepares the config map
 *  It spin's of a pod to do the job
 *  It monitors the pod for errors
 *  Feedback to gitlab on problems with pod, not he job!
 *
 *  \param runner the runner data (which gitlab ..etc)
 *  \param parameters the global dispatcher parameters
 *  \return I have no clue yet
 */
int do_job_oc(cJSON * runner,cJSON* job,parameter_t * parameters );

#endif /* defined(__deployctl__oc_do_job__) */
