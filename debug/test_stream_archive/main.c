//
//  main.c
//  test_stream_archive
//
//  Created by Danny Goossen on 22/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include "../src/libarchive_api.h"
#include "../src/openssl.h"
#include "c-ares.h"
#include "../src/error.h"

int main(int argc, const char * argv[]) {
	// insert code here...
	printf("Hello, World!\n");
	
	
	thread_setup(); //TODO move this to main

	curl_global_init(CURL_GLOBAL_ALL); //TODO move this to main

#ifndef __APPLE__
	ares_library_init(ARES_LIB_INIT_ALL );
#endif
	
  layer_data_t * layer_data=init_layer_data(0x10000);
	
	
	 char * const * pathnames=(char * const[]){
		"/build/cache/base/packages",
		"/build/cache/updates/packages",
		NULL};
	
	setdebug();
	char * location=stream_archive(pathnames,2 ,"/", stream_source_rootfs, layer_data, output_mode_file,chmod_u2g, NULL, "/build/test.tar.gz");
	if (location) free(location);
	free_layer_data(&layer_data);
#ifndef __APPLE__
	if (ARES_SUCCESS==ares_library_initialized()) ares_library_cleanup();
#endif
	
	curl_global_cleanup(); //TODO move this to main
	thread_cleanup(); //TODO move this to main
	
    return 0;
}
