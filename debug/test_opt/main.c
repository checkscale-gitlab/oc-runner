//
//  main.c
//  test_opt
//
//  Created by Danny Goossen on 6/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include <strings.h>
#include <getopt.h>
#include "error.h"
#include <stdlib.h>
#include <stdarg.h>

/**
 \brief typedef parameter structure, \b  dkr_parameter_s
 */
typedef  struct dkr_parameter_s dkr_parameter_t;


/**
 \brief parameter structure, general sub-program settings
 Parameter settings for the dkr API sub programs
 */
struct dkr_parameter_s
{
	u_int8_t verbose; /**< verbose output */
	u_int8_t quiet;   /**< no output */
	u_int8_t u2g;   /**< copy user permissions to group */
	u_int8_t allow_fail;   /**< if set, on failure, exit code will be 0 */
	u_int8_t GLR;       /**< is GLR registry */
	u_int8_t ISR;       /**< is ISR registry */
	char * rootfs;        /**< absolute path to rootfs to transform to docker image */
	char * content;       /**< what content to add to the layer, defaults to "." */
	char * image_name;    /**< imagename, as subspacename of the oc runner e.g. $PROJECT_PATH_NAME_SLUG */
	char * reference;     /**< tag reference to push image*/
	char * namespace;     /**< namespace of the registry (namespace oc) */
	char * registry_name; /**< for imagestream ip:5000 */
	char * image;         /**< full registry image name */
	char * image_nick;    /**< image nick name e.g. ImageStream/test:latest */
	char * image_dir;     /**< directory for image layer.tar.gz / config / manifest */
	char * layer_tar_gz;  /**< Archive name */
	char * tag;           /**< tag reference */
	char * creds;         /**< credentials base64 usr/token */
	const char * layer_content_digest;
	const char * layer_blob_digest;
	size_t layer_size;
	
	
};
static int process_options_registry_push(size_t argc, char **argv, dkr_parameter_t *parameters);

int process_options_registry_push(size_t argc, char **argv, dkr_parameter_t *parameters)
{
	struct option long_options[] = {
		
		{ "verbose",      0, NULL, 'v' },
		{ "quiet",        0, NULL, 'q' },
		{ "u2g",          0, NULL, 'u' },
		{ "rootfs",       1, NULL, 'p' },
		{ "archive",      2, NULL, 'l' },
		{ "image-dir",    1, NULL, 'k' },
		{ "content",      1, NULL, 'c' },
		{ "name",         1, NULL, 'm' },
		{ "reference",    1, NULL, 's' },
		{ "image",        1, NULL, 'j' },
		{ "GLR",          0, NULL, 'h' },
		{ "credentials",  1, NULL, 'd' },
		{ NULL,           0, NULL,  0  }  };
	
	int           which;
	parameters->ISR=0;
	parameters->GLR=0;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameters->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameters->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameters->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameters->quiet = 1;
				}
					break;
				case 'l': {
					if(optarg)
					{
					parameters->layer_tar_gz =calloc(1,strlen(optarg)+1);
					sprintf(parameters->layer_tar_gz,"%s",optarg);
					}
					else
					{
						parameters->layer_tar_gz =calloc(1,strlen("Manao")+1);
						sprintf(parameters->layer_tar_gz,"Manao");
					}
				}
					break;
				case 'u': {
					parameters->u2g=1;
				}
					break;
				case 'p': {
					parameters->rootfs =calloc(1,strlen(optarg)+1);
					sprintf(parameters->rootfs,"%s",optarg);
				}
					break;
				case 'c': {
					parameters->content =calloc(1,strlen(optarg)+1);
					sprintf(parameters->content,"%s",optarg);
				}
					break;
				case 'm': {
					parameters->image_name =calloc(1,strlen(optarg)+1);
					sprintf(parameters->image_name,"%s",optarg);
				}
					break;
					
				case 's': {
					parameters->reference =calloc(1,strlen(optarg)+1);
					sprintf(parameters->reference,"%s",optarg);
				}
					break;
				case 'j': {
					parameters->image =calloc(1,strlen(optarg)+1);
					sprintf(parameters->image,"%s",optarg);
				}
					break;
					
				case 'd': {
					parameters->creds =calloc(1,strlen(optarg)+1);
					sprintf(parameters->creds,"%s",optarg);
				}
					break;

				case 'h': {
					parameters->GLR=1;
					parameters->ISR=0;
				}
					break;
					/* otherwise    : display usage information */
				default:
					;
					break;
			}
		}
	}
	if (!parameters->image && !parameters->GLR) parameters->ISR=1;
	if (parameters->image) parameters->GLR=0;
	return optind;
}

char * avprintf(const char * format, va_list vargs);

#define CSPRINTF(x) ({va_list vargs; va_start(vargs, x); cvaprintf(x,vargs);})


char * cvsprintf(const char *  format , va_list vargs)
{
	va_list vargf;
	va_copy(vargf, vargs);
	int size=vsnprintf(NULL,0 ,format,vargs);
	va_end(vargs);
	if (size<0) return NULL; else size++;
	
	char * result=calloc(1,size);
	size=vsnprintf(result,size ,format,vargf);
	va_end(vargf);
	if (size<0)
	{
		if (result) free(result);
		result=NULL;
	}
	return result;
}

char * test_CSPRINTF(const char * output_path,...);

char * test_CSPRINTF(const char * filename,...)
{
	char * tt=cvsprintf;
	return(tt);
}

int main(int argc, char * argv[]) {
	// insert code here...
	printf("Hello, World!\n");
	//dkr_parameter_t * parameters=calloc(1,sizeof(dkr_parameter_t));
	//int res=process_options_registry_push(argc, argv, parameters);
	//printf("ret=%d\n",res);
	
	char * o=test_CSPRINTF("test/%s/my","mytestggg");
	printf("%s\n",o);
	
	
    return 0;
}
