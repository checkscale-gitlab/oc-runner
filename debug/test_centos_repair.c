//
//  test_centos_repair.c
//  oc-runner
//
//  Created by Danny Goossen on 30/7/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "../src/deployd.h"
#include "../src/cJSON_deploy.h"
#include "../src/yaml2cjson.h"
#include "../src/docker_config.h"
#include "../src/utils.h"
#include "../src/autolabel.h"


int main(int argc, char * argv[]) {
	// insert code here...
	printf("Hello, World!\n");
	//dkr_parameter_t * parameters=calloc(1,sizeof(dkr_parameter_t));
	//int res=process_options_registry_push(argc, argv, parameters);
	//printf("ret=%d\n",res);
	
	cJSON * labels=cJSON_CreateObject();
	cJSON_add_string(labels, "org.label-schema.schema-version", "= 1.0   org.label-schema.name=CentOS Base Image      org.label-schema.vendor=CentOS        org.label-schema.license=GPLv2 org.label-schema.build-date=20180531" );
	cJSON_add_string(labels, "org.label-schema.schema-version", "1.0");
	setdebug();
	parse_faulty_centos_labels(labels);
	print_json(labels);
	
	return 0;
}
