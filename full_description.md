

# oc-runner

[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

a custom gitlab-ci-multi-runner for building on openshift cluster with some bells and whistles.

## Purpose

 In todays world of GitOps, it's important to be able to build fast to enable a truly Continious Integration Development Process.

  Oc-runner comes with some specific features to realize the fast builds, without compromising network usage, extra load on the git repository nor the security of the build platform by running root-less or non root:
  - Create a custom build image that can build and/or test with all dependencies present, so no more:
     - sudo apt-get install / or sudo yum install to load dependend packages
     - or bundle install to prepare a ruby environment 
     - ...
  - Use of WORK_SPACES when a specific build image is not an option, that can be used during a pipeline.
  - Use of GIT_CACHE to lower resources usage.
  - Use WORK_SPACES as a cache, to cache repositories/downloads ...etc

 Been able to push directly from a file system to a registry, without loading a specific image, means you can compile or build your project with a specific image, create a directory as rootfs and push, tag update dockerHub descriptions... etc directly from within the build.

 Although oc-runner build-in commands are in no way a replacement for docker build with Dockerfile, since we cannot do the `RUN` command, there is still a lot that can be done with some simple tricks and tools.

 The main goal for oc-runner is and stays being able to Continious Integrate, Continious Deliver / Release from a git Repository.
 
  GitOps all the way from custom buildimages, Toolchains and application image, on an Openshift platform, using all the features and advantages from Gitlab and Gitlab-ci, with a super minimum of maintenance, there is only one token to register the runner on install.

### Features of the runner: 
 
1. integrated commands for registry operations: `registry_push [options] [pathname ...]`
    - Basic operations:
        - a `rootfs` to registry [with config] 
        - an `archive` to registry [with config]
        - transfer [with config] from an image to an image
        - transfer [with config] from an image and append layer from `rootfs` or `archive` to an image
     - Features   
	    - Autolabeling: 
            - Adds Meta Data as LABEL to Container Config [org.opencontainer.img](https://github.com/opencontainers/image-spec/blob/master/annotations.md#rules)
            - Best effort to create a minimun set of Meta Data at all time.
        - Optimised disk usage
            - transfers happen in memory chunks, multithreaded.
            - from rootfs compression, calculate content and blob digist to registry in stream chunks, no disk usage!
            - from archive, calculate content and blob digest to registry in stream chunks, no disk usage!
        - config.yml yaml or json to define the manifest config
        - owner ship of files converted to root:root
        - optional change permision of `group` to match `owner`.
        - auto login with known registries for pulling job image('s') and integrated registy-commands
            - Gitlab Registry, by ci_token 
            - ImageStream, openshift internal registy, by service token
            - if `DOCKER_CREDITIALS` defined, the DockerHub
2. integrated commands for registry remote image tagging
    - for DockerHub or custom registry:
    ```bash
registry_tag_image --image=<image> --tag=<NEW_TAG>`
```
    - for Internal registry openshift image as: ImageStream/name:reference
    ```bash
registry_tag_image --ISR [--name=<name>] [--reference=<reference>]--tag=<NEW_TAG>
```    
    - for Gitlab registry, image as:
        -  Gitlab/name:reference
        - `${CI_REGISTRY_IMAGE/name:reference}`
    ```bash
registry_tag_image --GLR [--name=<name>] [--reference=<reference>]--tag=<NEW_TAG>
```
3. integrated command to delete images on openshift registry:
	- ImageStream_delete [--name=<name>]
	- ImageStream_delete_tag [--name=<name>] [--reference=<reference>]
4. integrated command `PostHook url` creates a POST request to `url`
5. Integrated DockerHub commands
    - `DockerHub_set_description`
    - `DockerHub_delete_tag`
    - `DockerHub_delete_repository` 
6. Integrated `MicroBadger_Update --image=namespace/image` command.
    - see [MicroBadger](https://microbadger.com/)
7. Use of `WORK_SPACES` with Openshift internal ImageStream registry backend storage.
    Use as cache or WORK_SPACE:    
    - scope 
        - global
        - group namespace
        - project
        - pipeline
    - path: filepath to use as workspace
    - environment, list of environment variables for workspace
8. Use of `GIT_CACHE` to reduce bandwidth and storage for git operations, also as per previous point, stored as image in ImageStream.
9. Parallel downloads of Gitlab-ci Artifacts
10. integrated command **`copy`**
    - quicly substitude text with environment vars [--substitute]
    - copy file or text to terminal,file or environment variable
    - conditionale execution:
        - `--if_zero="$${var#test}"`
        - `--if_value="$${var#test2}"`
11. Zero *image* requirements for running a JOB, can use `scratch`, since all essential commands are integrated in the executor. 
    - libgit2 for git
    - curl for all api commands
    - libares for dns resolution
    - libarchive for compresing and streaming archives
    - openssl for secure connections
    - libzip for compressing and decompresing artifacts
    - libz / libbzip2 as compressor for libarchive

## install
 
see [manual install](https://oc-runner.deployctl.com/latest/files/manual/manual.html#install)

## Resources

- [manual](https://oc-runner.deployctl.com/latest/files/manual/manual.html)

- [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

- [Releases](https://oc-runner.deployctl.com/latest)