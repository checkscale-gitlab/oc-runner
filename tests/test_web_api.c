/*
 test_web_api.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
#include "../src/deployd.h"
#include "test_web_api.h"


/*------------------------------------------------------------------------
 *
 * Signal handler
 *
 *------------------------------------------------------------------------*/

static void signal_handler(sig)
int sig;
{
   int i;
   switch(sig) {

      case SIGSTOP:
      case SIGQUIT:
      case SIGINT:
      case SIGTERM:
      case SIGSEGV:
      	debug ("\nsighandler: Signal %d : %s !!!\n",sig,strsignal(sig));
         for (i=getdtablesize();i>=0;--i)
         {
            close(i); /* close all descriptors */
         }
         exit(0);
         break;
   }
// LCOV_EXCL_START
// should never come here, and if, makes nod difference
}
// LCOV_EXCL_STOP
/**********************************************************************************
 * Test web server, returns 0 on success.
 * forks and keeps running in background with pid: child_pid
 *       Listens on localhost:<port>
 *
 * Stop with : stop_test_web(child_pid);
 *
 * webserver contains 4 buffers
 *
 * char request[1024] => format with %d for port in Host header:
 * e.g: "GET /test "HTTP/1.1\r\nHost: localhost:%d\r\n .... "
 *
 * char reply_header[1024]: format as printf=> first %d for port, %d for content length, %s for content
 * e.g : "HTTP/1.1 200 OK\r\nServer: testweb/0.0.1\r\nContent-Length:%d\r\n\r\n%s"
 * char reply_content[1024]; content as per above
 *
 * char reply_fail[1024] :=> response on failure
 *
 * if request == request=> response is reply... else reply_fail
 *
 **********************************************************************************/


int webserver( pid_t * child_pid, u_int16_t * port, webserver_t * webserver, int id)
{
   // first bind!
   int bind_result=0;
   int fd = socket(AF_INET, SOCK_STREAM, 0);

   struct sockaddr_in serv_addr;
   bzero((char *) &serv_addr, sizeof(serv_addr));

   // configure server information
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
   serv_addr.sin_port = ntohs(0);
   bind_result=bind(fd,(struct sockaddr *) &serv_addr, sizeof(serv_addr));
   if (bind_result!=SOCKET_ERROR )
   {
      socklen_t size = sizeof(serv_addr);
      getsockname(fd, (struct sockaddr *) &serv_addr, &size);
      *port = ntohs(serv_addr.sin_port);
      // then fork
      *child_pid = fork();

      if(*child_pid == 0)
      {
         //the child
         int fds=INVALID_SOCKET;
         signal(SIGKILL,&signal_handler); /* catch kill signal */
         signal(SIGTERM,&signal_handler); /* catch kill signal */
         signal(SIGSTOP,&signal_handler); /* catch kill signal */
         signal(SIGQUIT,&signal_handler); /* catch kill signal */
         signal(SIGSEGV,&signal_handler); /* catch kill signal */
         // socket and bind
         listen(fd, 1);
         int accept_error=0;
         while (1) {
            fds=accept(fd, NULL,NULL);
            if (fds==INVALID_SOCKET ) accept_error=errno;
            if (fds==INVALID_SOCKET && accept_error==EINTR) continue;
            if (fds==INVALID_SOCKET && accept_error!=EINTR) break;
            debug("child: got a connection\n");
            size_t read_size=0;
            size_t write_size=0;
            char * buff[0x10000];

            usleep(1000); // make sure client has sent it!! so only one read for the whole buff.
            do{
               read_size=recv(fds, buff, 0x10000, 0);
            }
            // LCOV_EXCL_START
            //  never get to it
            while (read_size==0 && (read_size!=SOCKET_ERROR || (read_size==SOCKET_ERROR && errno==EINTR) ));
            // LCOV_EXCL_STOP

            buff[read_size]=0;
            debug("Child : received \n>>>%s<<<\n",buff);
            // get the port correct in request
            char temp[1024];
            sprintf(temp, webserver->request,*port);
            debug("Child : >>>cmp2\n>>>%s<<<\n==\n>>>%s<<<\n",temp,buff);
            if (strcmp((const char* )buff, temp)==0){
               sprintf((char *)buff, webserver->reply_header,strlen(webserver->reply_content),webserver->reply_content);
            }
            else {
               sprintf((char *)buff, "%s",webserver->reply_fail);
            }
            write_size=write(fds,(char *)buff,strlen((char*)buff));
            read_size=0;
         }
          // LCOV_EXCL_START
         // should never come here
         exit(0);
         // LCOV_EXCL_STOP
      }
      else if (*child_pid>0)
      {
      // the parent
         //close the accept filehandle and return return
         close(fd);
         return 0;
      }
   }
   // LCOV_EXCL_START
   // should never come here
   return -1;
   // LCOV_EXCL_STOP
}

int stop_test_web(pid_t child_pid)
{
   //kill
   kill(child_pid,SIGQUIT); // TODO need to check with WHOANG
   usleep(100000);
   int status=0;
   pid_t w = waitpid(child_pid,&status, 0);
   if (w==-1) { sock_error("waitpid"); return 1; }
   else if (WIFEXITED(status))
   {
       error("exit webserver w error: %d\n",WEXITSTATUS(status));
   }
   // LCOV_EXCL_START
   // we never get here
   else { return 1; }
   // LCOV_EXCL_STOP
   return 0;
}
